import { Component, Input } from '@angular/core';

import { PostComponent } from '../../post.component';

@Component({
  templateUrl: 'html-post.component.html',
  selector: 'html-component-post'
})
export class HtmlPostComponent implements PostComponent {
  @Input() post: any;
  @Input() params: any;
}
