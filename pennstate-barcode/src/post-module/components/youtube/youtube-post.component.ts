import { Component, Input } from '@angular/core';

import { PostComponent } from '../../post.component';

@Component({
  templateUrl: 'youtube-post.component.html',
  selector: 'youtube-post.component'
})
export class YouTubePostComponent implements PostComponent {
  @Input() post: any;
  @Input() params: any;

}
