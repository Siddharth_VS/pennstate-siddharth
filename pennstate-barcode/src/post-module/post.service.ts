import { Injectable } from '@angular/core';
import { BannerPostComponent } from './components/banner/banner-post.component';
import { PollPostComponent } from './components/poll/poll-post'
import { BannerWithDescriptionComponent } from './components/banner-with-description/banner-with-description-post-component';
import { CalendarPostComponent } from './components/calendar/calendar-post-component';
import { PostItem } from './post-item';
import { UserData } from '../providers/user-data';
import * as moment from 'moment-timezone';
import { AngularFire } from 'angularfire2';
import { YouTubePostComponent } from './components/youtube/youtube-post.component';
import { HtmlPostComponent } from './components/html/html-post.component';
import { ImageGalleryPostComponent } from './components/image-gallery/image-gallery-post.component';

@Injectable()
export class PostService {

  constructor(
    public af: AngularFire,
    public userData: UserData,
  ) { }

  components: any = {
    'BANNER': BannerPostComponent,
    'BANNER_WITH_DESCRIPTION': BannerWithDescriptionComponent,
    'CALENDAR': CalendarPostComponent,
    'POLL': PollPostComponent,
    'YOUTUBE': YouTubePostComponent,
    'HTML': HtmlPostComponent,
    'IMAGE_GALLERY': ImageGalleryPostComponent
  };

  getComponent(postData: any) {
    return new Promise((resolve, reject) => {
      resolve(new PostItem(this.components[postData.type], postData));
    })
  }

  savePost(post: any, published: boolean, uid: string, edit: any) {
    return new Promise((resolve, reject) => {
      let id = (+ moment.utc().format('x'))
      if (edit) {
        post.modifiedDate = (+ moment.utc().format('x'));
      }
      else {
        post.createdDate = post.modifiedDate = (+ moment.utc().format('x'));
        post.createdBy = uid;
      }
      if ((!!!post.published && published) || post.type == 'CALENDAR') {
        post.published = true;
        post.publishedTime = (+ moment.utc().format('x'));
      }
      if (post.type === "POLL") {
        post.modifiedBy = post.createdBy;
      }
      if (edit) {
        this.af.database.object('/posts/' + post.id)
          .remove().then(() => {
            post.id = id;
            this.af.database.object('/posts/' + id)
              .set(post)
              .then(() => {
                resolve();
              })
          })
      } else {
        post.id = id;
        this.af.database.object('/posts/' + id)
          .set(post)
          .then(() => {
            resolve();
          })
      }
    })
  }

}
