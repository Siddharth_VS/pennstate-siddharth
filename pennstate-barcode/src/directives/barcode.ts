import { Directive, Input, ElementRef, AfterContentChecked } from '@angular/core';
import JsBarcode from 'jsbarcode';

@Directive({
  selector: '[barcode]'
})

export class Barcode implements AfterContentChecked {
  constructor(
    public el: ElementRef,
  ) { }
  @Input('barcode') barcode: any;

  ngAfterContentChecked() {
    JsBarcode(this.el.nativeElement, this.barcode, {
      width: 3,
      height: 80,
      marginLeft: 20,
      marginRight: 20,
      textAlign: "center",
      displayValue: true
    });
  }
}
