import { Directive, ElementRef, Input } from '@angular/core';
import * as ImgCache from 'imgcache.js';
import { ImageCacheService } from '../providers/image-cache-services';

@Directive({
  selector: '[imageCache]',
  providers: [ImageCacheService]
})

export class ImageCacheDirective {

  constructor(
    private el: ElementRef,
    public imgCacheService: ImageCacheService
  ) { }

  @Input('imageCache') src: string;

  ngOnChanges() {
    if (this.src) {
      this.imgCacheService.isCached(this.src)
        .then((res) => {
          if (res) {
            ImgCache.useCachedFile(this.el.nativeElement);
          }
          else {
            this.imgCacheService.cacheFile(this.src)
          }
        });
    }
  }

}
