import { Injectable } from '@angular/core';
import { ApiService } from './apiService';

@Injectable()
export class BaseClass {
  public key: string;
  public url: string;

  constructor(
    public apiService: ApiService
  ) { }

  public find(query?:any) {
    if(query){
      let url = this.url + '?$filter=' + query;
      return this.apiService.find(url);
    } else {
      return this.apiService.find(this.url);
    }
  }

  public findById1(value: any) {
    return this.apiService.findBy({
      url: this.url,
      key: this.key,
      value: value
    });
  }

  public findById(value: any) {
    return this.findBy({
      url: this.url,
      key: this.key,
      value: value
    });
  }

  public findBy(request: any) {
    request.url = this.url;
    return this.apiService.findBy(request);
  }

}
