import { App } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { InAppBrowser } from 'ionic-native';

@Component({
  selector: 'files',
  templateUrl: 'files.html',
  providers: [HelperService]
})
export class Files {
  @Input('params') args: any;
  @Output() removeFile = new EventEmitter();
  file?: any = {};
  fileSize: any;
  extension: any;
  documentFileTypes: string;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public helper: HelperService
  ) { }

  ngOnChanges() {
    this.afoDatabase.object('/settings/documentFileTypes')
      .take(1)
      .subscribe(types => {
        this.documentFileTypes = types.$value ? types.$value : ".xlsx, .xls, .doc, .docx,.ppt, .pptx,.txt, .pdf";
      });

    this.afoDatabase.object('/files/' + this.args.folder + '/' + this.args.id)
      .subscribe((file) => {
        this.file = file;
        this.fileSize = this.helper.formatFileSize(this.file.size);
        this.extension = this.file.fileName ? this.file.fileName.substr(this.file.fileName.lastIndexOf('.') + 1) : 'file';
      });
  }

  viewFile(file: any) {
    if (this.args.view == true) {
      if (this.documentFileTypes.indexOf(this.extension) > -1) {
        new InAppBrowser('https://docs.google.com/gview?embedded=true&url=' + encodeURIComponent(file.fileUrl), '_blank', 'location=yes,closebuttoncaption=Done,toolbar=yes,presentationstyle=pagesheet,transitionstyle=fliphorizontal');
      }
      else {
        new InAppBrowser(file.fileUrl, '_blank', "location=yes,toolbar=yes,mediaPlaybackRequiresUserAction=yes, allowInlineMediaPlayback=yes");
      }
    }
  }

  deleteFile(fileId: any) {
    this.removeFile.emit({ id: fileId });
  }
}
