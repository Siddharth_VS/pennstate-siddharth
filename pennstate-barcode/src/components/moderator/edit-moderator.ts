import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { AngularFire } from 'angularfire2';
import * as _ from 'underscore';

@Component({
  selector: 'edit-moderator',
  templateUrl: 'edit-moderator.html'
})

export class EditModerator {
  user: any = {};
  temp: any;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public navCtrl: NavController
  ) {
    let self = this;
    this.helperService.getUid()
      .then((uid: any) => {
        self.afoDatabase.object('/users/' + uid)
          .subscribe((snapshot: any) => {
            this.user = {
              email: snapshot.email,
              firstName: snapshot.firstName,
              lastName: snapshot.lastName,
              uid: snapshot.uid,
              password: '',
              confirmPassword: '',
              newEmail: ''
            }
            this.temp = Object.assign({}, this.user);
          })
      });
  }

  updateUser() {
    this.helperService.showLoading();

    if (this.user.password != this.user.confirmPassword) {
      this.helperService.hideLoading();
      this.helperService.showMessage('Passwords do not match');
      return false;
    }

    if (this.user.password && this.user.password.length && this.user.confirmPassword && this.user.password.length < 6 && this.user.confirmPassword.length < 6) {
      this.helperService.hideLoading();
      this.helperService.showMessage('Password must be atleast 6 characters');
      return false;
    }

    if (this.user.password && this.user.password.length) {
      this.helperService.resetPassword(this.user.email, this.user.password)
        .catch((errRes: any) => {
          this.helperService.hideLoading();
          return false;
        })
    }

    delete this.user.password;
    delete this.user.confirmPassword;

    this.saveUser()
      .then(() => {
        let generatedOtp: string;
        if (this.user.newEmail && this.user.newEmail.length) {
          this.af.database.list('users', { query: { orderByChild: 'email', equalTo: this.user.newEmail.toLowerCase() } })
            .take(1)
            .subscribe((snapshots: any) => {
              if (snapshots && !snapshots.length) {
                this.helperService.sendOtp(this.user.newEmail, 'VerifyAccount')
                  .then((res) => {
                    generatedOtp = res.otp.slice(15, -7);
                    this.helperService.hideLoading();
                    this.navCtrl.push(LoginPage, { slideId: 2, email: this.user.newEmail.toLowerCase(), generatedOtp: generatedOtp });
                    this.user.newEmail = '';
                  })
                  .catch((err) => {
                    console.log('err occured', err);
                    this.helperService.hideLoading();
                    this.helperService.showMessage('Failed to update the User Details');
                  })
              }
              else {
                this.helperService.hideLoading();
                this.helperService.showMessage('Email already exists, please enter unique EmailId');
              }
            });
        }
        else {
          this.helperService.hideLoading();
          this.helperService.showMessage('User details updated successfully');
        }
      })
      .catch((err) => {
        console.log('err occured', err);
        this.helperService.hideLoading();
        this.helperService.showMessage('Failed to update the User Details');
      })
  }

  saveUser() {
    return new Promise((resolve, reject) => {
      this.afoDatabase.object('/users/' + this.user.uid)
        .update(this.user)
        .then(() => {
          this.temp = Object.assign({}, this.user);
          resolve();
        })
        .catch((err: any) => {
          reject(err);
        });
    })
  }

  isEqual(obj1: {}, obj2: {}) {
    return _.isEqual(obj1, obj2);
  }
}
