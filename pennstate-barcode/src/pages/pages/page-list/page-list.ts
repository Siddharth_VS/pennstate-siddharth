import { Component } from '@angular/core';
import { LoadingController, ModalController, NavController } from 'ionic-angular';
import { CreatePage } from '../page-add/page-add';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { CustomPage } from '../../custom-page/custom-page';
import * as _ from 'underscore';

@Component({
  selector: 'custom-pages-list',
  templateUrl: 'page-list.html'
})
export class PagesList {
  allPages: any[] = [];
  pages: any = [];
  category: any = 'ALL';
  categories: any;

  constructor(
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public afoDatabase: AngularFireOfflineDatabase,
    public navCtrl: NavController,

  ) {
    let loading = this.loadingCtrl.create();
    loading.present();

    afoDatabase.list('/settings/PageCategories')
      .take(1)
      .subscribe((categories) => {
        this.categories = [];
        this.categories = Object.assign([], categories);

        this.afoDatabase.list('/pages', { preserveSnapshot: true })
          .subscribe((snapshots) => {
            this.allPages = [];
            this.allPages = this.pages = Object.assign([], snapshots);
          });

        loading.dismiss();
      });

  }

  onChange(category: any) {
    let pos: any;
    this.pages = [];
    let self = this;

    if (category == 'ALL') {
      this.pages = this.allPages;
      return false;
    }

    this.afoDatabase.list('/pages', { query: { orderByChild: 'category', equalTo: category } })
      .subscribe((snapshots: any) => {
        snapshots.forEach(function(snapshot: any) {
          if (snapshot && snapshot.id) {
            pos = _.findIndex(self.pages, { 'id': snapshot.id });
            if (pos == -1) {
              self.pages.push(snapshot);
            }
          }
        });
      });
  }

  createPage() {
    this.navCtrl.push(CreatePage);
  }

  viewPage(pageId: any) {
    this.navCtrl.push(CustomPage, { pageId: pageId, showBackButton: true });
  }
}
