import { Component } from '@angular/core';
import { App, LoadingController, ModalController, NavParams, NavController, ToastController } from 'ionic-angular';
import { SpeakerAddPage } from '../speaker-add/speaker-add';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { FirebaseAnalyticsProvider } from '../../../providers/firebase-analytics-provider';
import { SpeakerDetailPage } from '../speaker-detail/speaker-detail';
import { AngularFire } from 'angularfire2';
import { UserData } from '../../../providers/user-data';
import * as _ from 'underscore';

@Component({
  selector: 'page-speaker-list',
  templateUrl: 'speaker-list.html',
  providers: [FirebaseAnalyticsProvider]
})

export class SpeakerListPage {
  personas: any[] = [];
  title: string;
  filterKeys = ["firstName", "lastName"]
  query: any = "";
  isAdmin: boolean = false;
  reorder: boolean = false;
  userRole: any;

  constructor(
    public af: AngularFire,
    public app: App,
    public afoDatabase: AngularFireOfflineDatabase,
    public fireAnalytics: FirebaseAnalyticsProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public userData: UserData
  ) {

    this.title = this.navParams.get('title') || 'Dignitaries';
    let loading = this.loadingCtrl.create();
    loading.present();
    fireAnalytics.setScreen('PERSONAS')


    this.userData.getRoleValue()
      .then((roleValue) => {
        this.afoDatabase.list('speakers', { query: { orderByChild: 'sortOrder' } })
          .subscribe((list) => {
            if (roleValue > 10) {
              this.personas = list;
              this.reorder = true
            }
            else {
              this.personas = list.filter(persona => persona.visibility == true);
            }
            loading.dismiss();
          });
      })
  }

  addPersona() {
    let modal = this.modalCtrl.create(SpeakerAddPage, { "speaker": {} });
    modal.present();
  }

  onItemReorder(indexes: any) {
    let element = this.personas[indexes.from];
    this.personas.splice(indexes.from, 1);
    this.personas.splice(indexes.to, 0, element);
  }

  // Order persona list
  savePersonas() {

    let loading = this.loadingCtrl.create();
    loading.present();

    let personaList = _.indexBy(this.personas, function(value, key) {
      value.sortOrder = key;
      delete value.$exists;
      delete value.$key;
      return value.id;
    });

    let self = this;
    this.af.database.object('/speakers/')
      .set(personaList)
      .then(function() {
        loading.dismiss();
        self.toastCtrl.create({
          message: 'Saved successfully',
          duration: 3000
        }).present();
      });
  }

  viewPersona(personaId: any) {
    this.app.getRootNav().push(SpeakerDetailPage, { speakerId: personaId });
  }

}
