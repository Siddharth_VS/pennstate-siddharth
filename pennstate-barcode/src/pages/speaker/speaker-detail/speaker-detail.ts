import { Component } from '@angular/core';
import { App, ActionSheetController, AlertController, Config, LoadingController, ModalController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
// import { SessionDetailPage } from '../../session/session-detail/session-detail';
import { SpeakerAddPage } from '../speaker-add/speaker-add';
import { HelperService } from '../../../providers/helperService';
import * as _ from 'underscore';

@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html',
  providers: [HelperService]
})

export class SpeakerDetailPage {
  speaker: any = { sessions: [] };

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public appCtrl: App,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public config: Config,
    public helper: HelperService,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController
  ) {
    let personaId = this.navParams.get('speakerId');
    if (personaId) {
      this.getPersona(personaId)
        .then(() => {
          if (this.speaker.sessionIds) {
            let promises: any = [];
            this.speaker.sessions = [];
            this.speaker.sessionIds.forEach((sessionId: any) => {
              let p = this.getSessionById(sessionId);
              p.then((session: any) => {
                if (session) {
                  this.speaker.sessions.push(session);
                }
              });
              promises.push(p);
            });

            Promise.all(promises)
              .then(() => {
                this.speaker.sessions = _.sortBy(this.speaker.sessions, 'startTime');
              }, (e) => {
                console.log(e);
              })
          }
        });
    }
  }

  // get persona
  getPersona(speakerId: any) {
    return new Promise((resolve, reject) => {
      this.afoDatabase.object('/speakers/' + speakerId, { preserveSnapshot: true })
        .subscribe((persona: any) => {
          if (persona.$exists()) {
            this.speaker = persona;
          }
          resolve();
        });
    });
  }

  // get all sessions that current persona is part of
  getSessionById(sessionId: any) {
    return new Promise((resolve, reject) => {
      this.afoDatabase.object('schedule/' + sessionId, { preserveSnapshot: true })
        .subscribe((session: any) => {
          if (session.$exists()) {
            session.startTime = this.helper.convertFromUtcToTz(session.startTime);
            session.endTime = this.helper.convertFromUtcToTz(session.endTime);
            resolve(session);
          } else {
            resolve();
          }
        });
    });
  }

  goToSessionDetail(session: any) {
    // this.navCtrl.push(SessionDetailPage, { sessionId: session.id });
  }

  editSpeaker() {
    let modal = this.modalCtrl.create(SpeakerAddPage, { "speakerId": this.speaker.id });
    modal.present();
  }

  showActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [{
        text: 'Edit Speaker',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          let modal = this.modalCtrl.create(SpeakerAddPage, { "speakerId": this.speaker.id });
          modal.present();
        }
      },
      {
        text: 'Delete Speaker',
        icon: !this.platform.is('ios') ? 'trash' : null,
        role: 'destructive',
        handler: () => {
          let self = this;
          this.helper.isAdmin()
            .then(() => {
              let confirm = this.alertCtrl.create({
                title: 'Are you sure ?',
                message: 'Deleting this cannot be undone?',
                buttons: [
                  {
                    text: 'Cancel'
                  },
                  {
                    text: 'Continue',
                    handler: () => {
                      let loading = this.loadingCtrl.create({});
                      return new Promise((resolve, reject) => {
                        if (self.speaker.sessionIds) {
                          self.speaker.sessionIds.forEach((sessionId: any) => {
                            this.af.database.object('/schedule/' + sessionId, { preserveSnapshot: true })
                              .subscribe((data) => {
                                let session = data.val();
                                let index: number;
                                if (session.personaIds) {
                                  Object.keys(session.personaIds).forEach((type: any) => {
                                    index = session.personaIds[type].indexOf(self.speaker.id);
                                    if (index > -1) {
                                      session.personaIds[type].splice(index, 1);
                                    }
                                  })
                                  this.af.database.object('/schedule/' + session.id)
                                    .set(session)
                                }
                              })
                          })
                        }
                        resolve()
                      }).then(() => {
                        self.af.database.object('speakers/' + self.speaker.id)
                          .remove().then(() => {
                            let toast = self.toastCtrl.create({
                              message: 'speaker deleted successfully',
                              duration: 3000
                            });
                            loading.dismiss();
                            toast.present();
                            self.viewCtrl.dismiss();
                          });
                      })
                    }
                  }
                ]
              });
              confirm.present();
            })
            .catch((err) => {
              console.log('err occured', err);
            })
        }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => { }
      }
      ]
    });
    actionSheet.present();
  }

  getSessionStatus(session: any) {
    if (this.helper.isSessionLive(session)) {
      return 'session-live';
    }
    else if (this.helper.isSessionOver(session)) {
      return 'session-over';
    }
    return '';
  }
}
