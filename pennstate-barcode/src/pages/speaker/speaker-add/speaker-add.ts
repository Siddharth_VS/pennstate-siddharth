import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AngularFire } from 'angularfire2';
import { HelperService } from '../../../providers/helperService'
import { App, AlertController, LoadingController, NavParams, ViewController, ModalController, NavController, ToastController } from 'ionic-angular';
import { FileUploader } from '../../file-uploader/file-uploader';

@Component({
  selector: 'page-speaker-add',
  templateUrl: 'speaker-add.html',
  providers: [HelperService]
})
export class SpeakerAddPage {
  speaker?: any = { visibility: false };
  submitted = false;
  edit: any;
  constructor(
    public af: AngularFire,
    public appCtrl: App,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public helper: HelperService
  ) {
    let self = this;
    if ((this.navParams.get('speakerId')) !== undefined) {
      this.speaker.id = this.navParams.get('speakerId');
      self.edit = true
      this.af.database.object('/speakers/' + this.speaker.id, { preserveSnapshot: true })
        .subscribe((speaker) => {
          this.speaker = speaker.val() ? speaker.val() : {};
        })
    } else {
      self.edit = false
    }
  }

  onSubmit(form: NgForm) {
    this.submitted = true;
    let self: any = this;
    if (form.valid) {
      let loading = this.loadingCtrl.create();
      loading.present();
      if (this.edit != true) {
        let childRef = this.af.database.list('/speakers').push({});
        (<any>this.speaker).id = childRef.key;
        this.speaker.sortOrder = Number.MAX_SAFE_INTEGER;

        childRef.set(this.speaker)
          .then(function() {
            let msg = 'speaker added successfully';
            let toast = self.toastCtrl.create({
              message: msg,
              duration: 3000
            });
            loading.dismiss();
            toast.present();
            self.dismiss();
          });
      } else {

        this.af.database.object('/speakers/' + this.speaker.id)
          .set(this.speaker).then(function() {
            let msg = 'speaker edited successfully';
            let toast = self.toastCtrl.create({
              message: msg,
              duration: 3000
            });
            loading.dismiss();
            toast.present();
            self.dismiss();
          });
      }
    }
  }
  changeImage(speaker: any) {
    let modal = this.modalCtrl.create(FileUploader, { folder: "speakers" });
    modal.present();

    modal.onDidDismiss((url: any) => {
      if (url) {
        speaker.picture = url;
      }
    });
  }

  dismiss(data: any) {
    this.viewCtrl.dismiss();
  }

}
