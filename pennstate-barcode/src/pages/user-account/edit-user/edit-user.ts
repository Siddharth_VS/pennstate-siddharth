import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { App, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { HelperService } from '../../../providers/helperService';
import { UserData } from '../../../providers/user-data';
import { EventListPage } from '../../event-list/event-list';
import { ModeratorList } from '../../moderator-list/moderator-list';

@Component({
  selector: 'page-user-datail-form',
  templateUrl: 'edit-user.html',
})

export class UserDetailFormPage {
  user: any = {};
  uid: string = '';
  isNewUser: any;

  constructor(
    public af: AngularFire,
    public events: Events,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public helperService: HelperService,
    public userData: UserData
  ) {
    this.user = navParams.get('user') || {};
    this.isNewUser = navParams.get('isNewUser') || false;
  }

  saveUserDetails() {
    if (this.user.password !== this.user.confirmPassword) {
      this.helperService.showMessage('Passwords do not match', 2000);
      return false;
    }
    this.helperService.showLoading();

    let self = this;
    this.helperService.createUser({ email: this.user.email.toLowerCase(), password: this.user.password })
      .then((res: any) => {

        let userData: any = {
          invitedDate: this.user.invitedDate,
          invitedBy: this.user.invitedBy,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
          appName: 'ADMIN'
        }
        if (this.user.inviteModifiedDate) {
          userData.inviteModifiedDate = this.user.inviteModifiedDate;
          userData.inviteModifiedBy = this.user.inviteModifiedBy;
        }

        self.af.database.object('/users/' + res.data.uid)
          .update(userData)
          .then(() => {
            return new Promise((resolve) => {
              self.afoDatabase.object('/events/' + this.user.uid)
                .subscribe((snapshots: any) => {
                  let eventIds: any = Object.keys(snapshots);

                  for (let id of eventIds) {
                    self.afoDatabase.object('/events/' + res.data.uid)
                      .update({ [id]: id });
                  }
                  resolve();
                });
            })
          })
          .then(() => {
            self.af.database.object('/users/' + this.user.uid)
              .remove()
              .then(() => {
                self.af.database.object('/events/' + this.user.uid).remove();
                self.helperService.showMessage('User details updated successfully');
                this.af.database.object('/users/' + res.data.uid)
                  .subscribe((user: any) => {
                    self.userData.login(user)
                      .then(() => {
                        self.helperService.hideLoading();
                        self.events.publish('user:login');
                        self.navCtrl.setRoot(user.roleValue == 99 ? EventListPage : ModeratorList)
                          .then(() => {
                            if (self.isNewUser) {
                              this.helperService.showMessage('Account created sucessfully', 2000);
                            }
                          });
                      })
                  })
              })
          });

      })
      .catch((errRes: any) => {

        if (errRes && errRes.error) {
          if (errRes.error.code == 'auth/invalid-password') {
            this.helperService.showMessage('Password must be atleast 6 characters');
          } else if (errRes.error.code == 'auth/email-already-exists') {
            this.helperService.showMessage('User already exists with this email address');
          }
        }
        this.helperService.hideLoading();
      });
  }

}
