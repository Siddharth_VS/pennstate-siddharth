import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { UserData } from '../../providers/user-data';
import { PostAddPage } from './post-add/post-add';

@Component({
  selector: 'page-manage-posts',
  templateUrl: 'posts.html'
})

export class PostsPage {
  posts: any[] = [];
  post?: any = {};
  roleValue: number;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userData: UserData
  ) {
    this.afoDatabase.list('posts', { query: { orderByChild: 'modifiedDate' } })
      .subscribe((posts) => {
        this.posts = posts.reverse();
        if (posts[0] && posts.length && posts[posts.length - 1]) {
          posts[0].modifiedDate > posts[posts.length - 1]
        }
      })
    this.userData.getRoleValue()
      .then((roleValue) => {
        this.roleValue = roleValue;
      })
  }

  addPost() {
    this.navCtrl.push(PostAddPage)
  }
}
