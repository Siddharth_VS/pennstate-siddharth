import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BarcodeScan } from './app.component';
import { MomentModule } from 'angular2-moment';
import { Settings } from '../constants/firebase-config';
import { Keyboard } from '@ionic-native/keyboard';
import { CodePush } from '@ionic-native/code-push';
import { Device } from '@ionic-native/device';
import { TinymceModule } from 'angular2-tinymce';
import { ChartsModule } from 'ng2-charts';

// providers
import { HelperService } from '../providers/helperService';
import { UserData } from '../providers/user-data';
import { FirebaseAnalyticsProvider } from '../providers/firebase-analytics-provider';
import { UtilProvider } from '../providers/util-provider';
import { ImageCacheService } from '../providers/image-cache-services';

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { Push } from '@ionic-native/push';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { AngularFireOfflineModule } from 'angularfire2-offline';
import { EmailComposer } from '@ionic-native/email-composer';
import { CookieModule } from 'ngx-cookie';

// pages
import { EventListPage } from '../pages/event-list/event-list';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { SendInvite } from '../pages/invites/send-invite/send-invite';
import { VerifyInvite } from '../pages/invites/verify-invite/verify-invite';
import { LoginPage } from '../pages/login/login';
import { UserDetailFormPage } from '../pages/user-account/edit-user/edit-user';
import { UserList } from '../pages/users/user-list';
import { AddUser } from '../pages/users/add-users/add-user';
import { AudienceListing } from '../pages/notifications/audience-listing/audience-listing';
import { NotificationsListPage } from '../pages/notifications/notification-list/notification-list';
import { SendNotificationsPage } from '../pages/notifications/send-notifications/send-notifications';
import { NotificationDetailPage } from '../pages/notifications/notification-detail/notification-detail';
import { MenuPage } from '../pages/menu/menu';
import { PagesList } from '../pages/pages/page-list/page-list';
import { CreatePage } from '../pages/pages/page-add/page-add';
import { CustomPage } from '../pages/custom-page/custom-page';
import { AlertModal } from '../pages/alert-modal/alert-modal';
import { ModeratorAlert } from '../pages/moderator-alert/moderator-alert';
import { FeedbackRatingListPage } from '../pages/feedbacks-ratings/feedback-rating-list/feedback-rating-list';
import { FeedbackRatingDetailPage } from '../pages/feedbacks-ratings/feedback-rating-detail/feedback-rating-detail';
import { FileUploader } from '../pages/file-uploader/file-uploader';
import { PostsPage } from '../pages/posts/posts';
import { Files } from "../components/files/files"
import { PostAddPage } from '../pages/posts/post-add/post-add';
import { ModeratorList } from '../pages/moderator-list/moderator-list';
import { UserListingPage } from '../pages/users/user-listing/user-listing';
import { UserMangementPage } from '../pages/users/user-management/user-management';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { SpeakerAddPage } from '../pages/speaker/speaker-add/speaker-add';
import { SpeakerDetailPage } from '../pages/speaker/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker/speaker-list/speaker-list';

// directives
import { ShowForDirective } from '../directives/show-for';
import { Barcode } from '../directives/barcode';
import { ProgressBar } from '../directives/progress-bar';
import { FireTrackDirective } from '../directives/fire-track';
import { ClickActionDirective } from '../directives/click-action';
import { PollProgressBar } from '../directives/poll-progress-bar';
import { BackgroundImage } from '../directives/background-image';
import { ImageCacheDirective } from '../directives/image-cache';

// components
import { UserCard } from '../components/user-card/user-card';
import { NoResults } from '../components/no-results/no-results';
import { TextAvatarDirective } from '../components/text-avatar/text-avatar.ts';
import { EditModerator } from '../components/moderator/edit-moderator';
import { MenuPages } from "../components/menuPages/menuPages/menuPages";
import { AddMenu } from '../components/menuPages/addMenu/addMenu';
import { AddIcon } from '../components/menuPages/add-icon/add-icon';
import { MomentAgo } from "../components/momentAgo/momentAgo";
import { Like } from '../components/like/like';
import { AttachFilesComponent } from '../components/attach-files/attach-files';
import { ImageGallery } from '../components/image-gallery/image-gallery';
import { EventFilter } from '../components/event-filter/event-filter';
import { SingleEvent } from '../components/single-event/single-event';
import { Persona } from "../components/persona/persona";
import { SocialLinksComponent } from '../components/social-links/social-links';
import { ProfileCardComponent } from '../components/profile-card/profile-card';

//pipes
import { SearchPipe } from '../pipes/search-filter';
import { YouTube } from '../pipes/youtube.ts';

//post-module
import { BannerPostComponent } from '../post-module/components/banner/banner-post.component';
import { PollPostComponent } from '../post-module/components/poll/poll-post';
import { PostCardComponent } from '../post-module/post-card.component';
import { BannerWithDescriptionComponent } from '../post-module/components/banner-with-description/banner-with-description-post-component';
import { CalendarPostComponent } from '../post-module/components/calendar/calendar-post-component';
import { PostDirective } from '../post-module/post.directive';
import { PostService } from '../post-module/post.service';
import { YouTubePostComponent } from '../post-module/components/youtube/youtube-post.component';
import { HtmlPostComponent } from '../post-module/components/html/html-post.component';
import { ImageGalleryPostComponent } from '../post-module/components/image-gallery/image-gallery-post.component';


export const firebaseConfig = Settings.firebaseApp;
export const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};
// crm
import { ApiService } from '../providers/crm/apiService';
import { ApiConfigurations } from "../providers/crm/apiConfig";
import { BaseClass } from '../providers/crm/base';
import { Events } from "../providers/crm/events";
import { Participants } from "../providers/crm/participants";
import { Contacts } from "../providers/crm/contacts";
import { Speakers } from "../providers/crm/speakers";
import { Talisma } from "../providers/crm/talisma";

@NgModule({
  declarations: [
    Like,
    SingleEvent,
    Persona,
    EventFilter,
    ImageGallery,
    PostsPage,
    PostAddPage,
    Files,
    AttachFilesComponent,
    ClickActionDirective,
    PollProgressBar,
    UserCard,
    NoResults,
    BarcodeScan,
    EventListPage,
    EventDetailPage,
    SendInvite,
    VerifyInvite,
    LoginPage,
    UserDetailFormPage,
    UserList,
    AddUser,
    ShowForDirective,
    Barcode,
    SocialLinksComponent,
    ProfileCardComponent,
    ProgressBar,
    TextAvatarDirective,
    EditModerator,
    SearchPipe,
    AudienceListing,
    NotificationsListPage,
    SendNotificationsPage,
    NotificationDetailPage,
    FeedbackRatingDetailPage,
    FeedbackRatingListPage,
    MenuPages,
    AddMenu,
    AddIcon,
    MomentAgo,
    MenuPage,
    PagesList,
    CreatePage,
    CustomPage,
    AlertModal,
    ModeratorAlert,
    FireTrackDirective,
    BannerPostComponent,
    PollPostComponent,
    PostCardComponent,
    BannerWithDescriptionComponent,
    CalendarPostComponent,
    PostDirective,
    YouTubePostComponent,
    HtmlPostComponent,
    SpeakerDetailPage,
    SpeakerListPage,
    SpeakerAddPage,
    ImageGalleryPostComponent,
    YouTube,
    FileUploader,
    ModeratorList,
    UserListingPage,
    UserMangementPage,
    NavigationDashboardPage,
    BackgroundImage,
    ImageCacheDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CookieModule.forRoot(),
    IonicModule.forRoot(BarcodeScan),
    AngularFireModule.initializeApp(firebaseConfig, firebaseAuthConfig),
    TinymceModule.withConfig({ 'auto_focus': false, }),
    AngularFireOfflineModule,
    MomentModule,
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    UserCard,
    SingleEvent,
    Persona,
    EventFilter,
    ImageGallery,
    PostsPage,
    PostAddPage,
    SocialLinksComponent,
    ProfileCardComponent,
    SpeakerDetailPage,
    SpeakerListPage,
    SpeakerAddPage,
    Files,
    BarcodeScan,
    EventListPage,
    EventDetailPage,
    SendInvite,
    VerifyInvite,
    LoginPage,
    UserList,
    AddUser,
    UserDetailFormPage,
    EditModerator,
    NotificationsListPage,
    SendNotificationsPage,
    NotificationDetailPage,
    FeedbackRatingDetailPage,
    FeedbackRatingListPage,
    MenuPages,
    AddMenu,
    AddIcon,
    AudienceListing,
    MenuPage,
    PagesList,
    CreatePage,
    CustomPage,
    AlertModal,
    BannerPostComponent,
    PollPostComponent,
    PostCardComponent,
    BannerWithDescriptionComponent,
    CalendarPostComponent,
    YouTubePostComponent,
    HtmlPostComponent,
    ImageGalleryPostComponent,
    FileUploader,
    ModeratorAlert,
    ModeratorList,
    UserListingPage,
    UserMangementPage,
    NavigationDashboardPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler },
    HelperService,
    UtilProvider,
    UserData,
    StatusBar,
    SplashScreen,
    Storage,
    BarcodeScanner,
    EmailComposer,
    Keyboard,
    CodePush,
    Device,
    ApiService,
    ApiConfigurations,
    Events,
    Participants,
    Contacts,
    Speakers,
    Talisma,
    BaseClass,
    FirebaseAnalyticsProvider,
    FirebaseAnalytics,
    Push,
    PostService,
    ImageCacheService
  ]
})
export class AppModule {
}
