(function() {
  'use strict';
  var functions = require('firebase-functions'),
    admin = require("firebase-admin"),
    moment = require('moment'),
    request = require('request-promise'),
    cors = require('cors')({
      origin: true
    }),
    settings = require("./settings.json"),
    serviceAccount = require("./serviceAccountKey.json"),
    _ = require('underscore-node'),
    nodemailer = require('nodemailer'),
    gmailEmail = encodeURIComponent(functions.config().gmail.email),
    gmailPassword = encodeURIComponent(functions.config().gmail.password),
    mailTransport = nodemailer.createTransport('smtps://' + gmailEmail + ':' + gmailPassword + '@smtp.gmail.com');
  // apiUserName = encodeURIComponent(functions.config().api.username);
  // apiPassword = encodeURIComponent(functions.config().api.password),
  // apiServer = encodeURIComponent(functions.config().api.server);

  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: settings.firebaseDatabaseURL
  });

  var db = admin.database();

  exports.Events = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      console.log(req.query);
      console.log('req.query.$filter', req.query.$filter);
      var $top = req.query.$top || '',
        $skip = req.query.$skip || '',
        filters,
        filterObj,
        filterObj2,
        $filter,
        $orderBy,
        orderByObj;

      if ((req.query.$filter || '').includes('and')) {
        filters = req.query.$filter.split(' and ');
        filterObj = ((filters[0] || '').trim()).replace(/[{()}]/g, '').split(' ');
        orderByObj = ((req.query.$orderBy || '').trim()).split(' ');
        console.log('filters[1].includes(contains)', filters[1].includes('contains'));
        console.log('filters[1]', filters[1]);
        if (filters[1].includes('contains')) {
          filterObj2 = ((filters[1] || '').trim()).replace(/[{()}]/g, '').split('contains');
        }
      } else {
        filterObj = ((req.query.$filter || '').trim()).replace(/[{()}]/g, '').split(' ');
        orderByObj = ((req.query.$orderBy || '').trim()).split(' ');

      }
      console.log('filterObj', filterObj);
      console.log('filterObj2', filterObj2);
      if (filterObj.length) {
        $filter = {
          key: filterObj[0],
          condition: filterObj[1],
          value: filterObj[2],
        };
        if (filterObj2 && filterObj2.length) {
          $filter.secondaryValue = filterObj2[1].split('Name,')[1].replace(/'/g, '');
          $filter.secondarycondition = 'Contains';
        }
      }
      console.log('$filter.secondaryValue', $filter.secondaryValue);
      if (orderByObj.length) {
        $orderBy = {
          key: orderByObj[0],
          order: orderByObj[1]
        };
      }

      var eventsRef = admin.database().ref('/events/');
      eventsRef.once("value", function(snapshot) {
        var result = [];

        if ($filter && filterObj2 && filterObj2.length) {
          result = _.filter(_.values(snapshot.val()), function(event) {
            if ($filter.condition != 'eq') {
              return ($filter.condition == 'ge' ? ((+moment(event[$filter.key]).format('x') > +moment($filter.value).format('x')) && (((event.Name || '').toLowerCase()).includes(($filter.secondaryValue || '').toLowerCase()))) :
                ((+moment(event[$filter.key]).format('x') < +moment($filter.value).format('x')) && (((event.Name || '').toLowerCase()).includes(($filter.secondaryValue || '').toLowerCase()))));
            } else {
              return event[$filter.key] == $filter.value;
            }
          });
        } else if ($filter) {
          result = _.filter(_.values(snapshot.val()), function(event) {
            if ($filter.condition != 'eq') {
              return $filter.condition == 'ge' ? ((+moment(event[$filter.key]).format('x') > +moment($filter.value).format('x'))) : (+moment(event[$filter.key]).format('x') < +moment($filter.value).format('x'));
            } else {
              return event[$filter.key] == $filter.value;
            }
          });
        }

        if ($orderBy) {
          result = _.sortBy(result, function(event) {
            return $orderBy.key;
          });
          result = $orderBy.order == 'desc' ? result.reverse() : result;
        }

        if ($skip) {
          result = result.splice($skip, result.length);
        }

        if ($top) {
          result = result.splice(0, ($top || result.length));
        }

        res.send({
          value: result
        });
      });
    });
  });

  exports.sendOtp = functions.https.onRequest(function(req, res) {
    var data = JSON.parse(req.body),
      email = data.email,
      msg = {},
      type = data.type,
      appType = data.appType;
    cors(req, res, function() {
      var otp = (Math.floor(Math.random() * 9000) + 1000);

      if (type == 'forgotPassword') {
        msg.subject = 'Inqude University Admin app - ' + otp + ' is an verification code to recover your account';
        otp = otp.toString().fontsize(7);
        msg.body = {
          line1: "You're receiving this email because you requested a password reset for your user account at Inqude University admin app",
          line2: " Enter the following verification code to reset your password.",
          otp: otp,
          footer: "Thank you,<br/>Support Team"
        };
      } else if (type == 'VerifyAccount') {
        if (appType == 'EVENT_APP') {
          msg.subject = 'Inqude University event app - ' + otp + ' is your verification code for secure access';
          msg.body = {
            line1: "You are just a step away from accessing your account, <br/><br/>We are sharing a verification code to access your account",
            line2: "The code is valid for 10 minutes and usable only once. <br/><br/>Once you are verified the code ,you'll be now able use the app.<br/><br/>Your OTP : <b>",
            otp: otp,
            footer: "<br/>Expires in : <b>10 minutes only</b><br/><br/>Best Regards,<br/>Support Team"
          };
        } else {
          msg.subject = 'Inqude University Admin app - ' + otp + ' is your verification code for secure access';
          msg.body = {
            line1: "You are just a step away from accessing your account, <br/><br/>We are sharing a verification code to access your account",
            line2: "The code is valid for 10 minutes and usable only once. <br/><br/>Once you are verified the code ,you'll be now able use the new email for login from next time.<br/><br/>Your OTP : <b>",
            otp: otp,
            footer: "<br/>Expires in : <b>10 minutes only</b><br/><br/>Best Regards,<br/>Support Team"
          };
        }

      } else {
        msg.subject = 'Inqude University Admin app - ' + otp + ' is your invitation code';
        msg.body = {
          line1: "You have been invited to Inqude University admin app",
          line2: "Enter the following Invite code along with your email address to start using the app",
          otp: otp,
          footer: "Thank you,<br/>Support Team"
        };
      }

      msg.html = `<table style="border-collapse: collapse; max-width: 400px; min-width: 300px; border: 1px solid silver;">
                    <tbody>
                      <tr style="height: 6px; background-color: #3366ff;">
                        <td style="width: 100%; text-align: center; height: 6px;">
                          <h2><span style="color: #ffffff;"Inqude University Admin app Invitation</span></h2>
                        </td>
                      </tr>
                      <tr style="text-align: center; height: 221px;">
                        <td style="width: 100%; height: 221px; padding: 5px 20px;">
                          <h3><strong>Greetings,</strong></h3>
                          <p>
                          ${msg.body.line1}.<br /><br />
                          ${msg.body.line2}
                          <br /><br />
                          <span style="font-size: 450%;">${msg.body.otp}</span><br /><br />
                          ${msg.body.footer}
                        </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>`

      sendEmail(email, msg).then(function(data) {
        if (data == 'success') {
          console.log('mail has been sent successfully');
          res.send({
            otp: otp,
            message: 'success'
          });
        } else {
          console.log('mail has been not sent' + data);
          res.send({
            message: 'sending mail failed' + data
          });
        }
      }).catch(function(error) {
        res.status(200).send({
          msg: 'sending mail failed' + error
        });
      });
    });
  });

  exports.sendEmailTicket = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      console.log(req.body);
      console.log(typeof(req.body));
      var data = req.body.data,
        email,
        msg = {},
        uid = data.uid,
        user = {},
        eventData = data.event;

      db.ref('users/' + uid)
        .once('value', function(snapshot) {
          user = snapshot.val();
          msg.subject = '[Inqude University Events] Event Ticket confirmation';
          msg.body = {
            line1: "You have been sucessfully purchased the ticket for the Event: " + eventData.Name,
            line2: "You can use this Email for Check-in process during the event.",
            footer: "Thank you,<br/>Support Team"
          };
          msg.html = `
    <table style="max-width: 400px;font-family:'Lato', sans-serif; min-width: 300px;;">
      <tbody>
        <tr style="height: 221px;">
          <td style="width: 100%; height: 221px; padding: 5px 20px;">
            <h3><strong>Greetings,</strong></h3>
            <p>
              ${msg.body.line1}.<br/><br/> ${msg.body.line2}
          <br/><br/>
          </p>
            <div style="width:450px ">
              <div class="container" style="display:flex;flex-direction:column;justify-content:center;width:100%;padding-bottom:20px;padding-top:20px;text-align:center;">
                <div class="inner-container" style="width:350px;box-shadow:0 0 0px #222;border-radius:15px;background: linear-gradient(to right, #E6BBAD, #e6adad);">
                  <widget type="ticket" class="main-ticket --flex-column" style="filter:drop-shadow(1px 1px 3px rgba(0, 0, 0, 0.3));padding:18px 0;">
                    <div class="top --flex-column" style="background-color:transparent;border-top-right-radius:5px;border-top-left-radius:5px;">
                      <div class="bandname -bold" style="font-weight:bold;padding-top:18px;">Event ticket</div>
                      <div class="tourname">${eventData.Name}</div>
                      <img class="picture" src="${eventData.EventImage}" alt="" style="width:100%;padding:18px 0;"/>

                      <div class="deetz --flex-row-j!sb" style="padding-bottom:18px;padding-bottom:10px !important;">
                        <div class="event --flex-column">
                          <div class="date">${moment(eventData.startDate).format('DD MMM, YYYY, hh:mm: A')}</div>
                          <div class="location -bold" style="font-weight:bold;">${eventData.EventVenue}</div>
                        </div>
                        <div class="price --flex-column">
                          <div class="label">Price</div>
                          <div class="cost -bold" style="font-weight:bold;">$ ${eventData.EventCost || 0}</div>
                        </div>
                      </div>
                    </div>
                    <div class="rip" style="background-color:#fff;height:0px;margin:0;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAACCAYAAAB7Xa1eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuOWwzfk4AAAAaSURBVBhXY5g7f97/2XPn/AcCBmSMQ+I/AwB2eyNBlrqzUQAAAABJRU5ErkJggg==);background-size:4px 2px;background-repeat:repeat-x;background-position:center;position:relative;box-shadow:0 1px 0 0 #fff, 0 -1px 0 0 #fff;background-image:linear-gradient(-45deg, #8067b7, #ec87c0) !important;">
                    </div>
                    <div class="bottom --flex-row-j!sb" style="border-top:4px dashed white;text-align:center;background-color:#fff;border-bottom-right-radius:5px;border-bottom-left-radius:5px;padding:18px;padding-top:10px;background:transparent !important;">
                      <img class="ticket-qr" src="https://api.qrserver.com/v1/create-qr-code/?data=${uid}&bgcolor=e6b0ad" style="width:120px;padding:5px;"/>
                   </div>
                  </widget>
                </div>
              </div>
              ${msg.body.footer}
      </div>
          </td>
        </tr>
      </tbody>
    </table>`
          sendEmail(user.email, msg).then(function(data) {
            if (data == 'success') {
              console.log('mail has been sent successfully');
              res.send({
                message: 'success'
              });
            } else {
              console.log('mail has been not sent' + data);
              res.send({
                message: 'sending mail failed' + data
              });
            }
          }).catch(function(error) {
            res.status(200).send({
              msg: 'sending mail failed' + error
            });
          });
        });
    });
  });

  var sendEmail = function(email, msg) {
    return new Promise(function(resolve, reject) {
      var mailOptions = {
        from: '"Inqude University Admin"<eventplex.campusmngmt@gmail.com>',
        to: email,
        subject: msg.subject,
        html: msg.html
      };
      mailTransport.sendMail(mailOptions).then(function() {
        resolve('success');
      }).catch(function(error) {
        reject('mail has been not sent' + error);
      });
    });
  };

  exports.createUser = functions.https.onRequest(function(req, res) {
    var userData;
    cors(req, res, function() {
      var user = req.body;
      admin.auth().createUser({
        email: user.email,
        password: user.password
      }).then(function(response) {
        userData = {
          email: user.email,
          uid: response.uid,
          roleValue: 20,
          createdDate: moment.utc().format('x'),
          status: 'accepted'
        };
        var userRef = db.ref('users/' + response.uid);
        return userRef.set(userData);
      }).then(function(response) {
        res.send({
          message: 'success',
          data: userData
        });
      }).catch(function(err) {
        console.log('err occured', err);
        res.send({
          message: 'failed to create user',
          error: err
        });
      });
    });
  });

  exports.resetPassword = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var data = JSON.parse(req.body);
      admin.auth().getUserByEmail(data.email).then(function(userRecord) {
        return admin.auth().updateUser(userRecord.uid, {
          email: data.email,
          password: data.password,
          disabled: false
        }).then(function(userRecord) {
          res.send({
            message: 'success',
            data: userRecord
          });
        }).catch(function(err) {
          res.send({
            message: 'failed to reset a password',
            error: err
          });
        });
      });
    });
  });

  exports.sendNotifications = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var message = req.body.message;
      var event = message.target == "Event Notification" && message.eventData ?
        message.eventData :
        '';
      console.log('message', message);
      var payload = {
          android: {
            data: {
              title: message.title,
              createdBy: message.createdBy,
              "content-available": "1",
              message: message.body,
              notId: '2',
              "body": message.body,
              "sound": "default",
              createdDate: message.createdDate,
              target: message.target || '',
              event: event,
              enableEventView: message.enableEventView || 'false',
              route: message.landingPage || 'EventListPage',
              editId: message.editId,
              click_action: 'FCM_PLUGIN_ACTIVITY'
            }
          },
          ios: {
            notification: {
              title: message.title,
              createdBy: message.createdBy,
              body: message.body,
              sound: "default",
              badge: "0",
              priority: 'high',
              notId: '2',
              createdDate: message.createdDate,
              target: message.target,
              route: message.landingPage,
              event: event,
              enableEventView: message.enableEventView || 'false',
              editId: message.editId
            }
          }
        },
        androidNotificationTokens = [],
        iosNotificationTokens = [];
      console.log('payload.android', payload.android);

      console.log('payload.ios', payload.ios);
      if (message.timeToLive) {
        payload.notification.timeToLive = (message.timeToLive * 60).toString();
      }
      if (message.requestAppRate === true) {
        payload.data.requestAppRate = 'true';
      }
      if (message.target == 'All Devices') {
        var targetRef = db.ref('/notificationTokens');

        targetRef.once('value', function(snapshots) {
          snapshots.forEach(function(snapshot) {
            if (snapshot.val() && snapshot.val().token.length > 0) {
              if (snapshot.val().type == 'android') {
                androidNotificationTokens.push(snapshot.val().token);
              } else {
                iosNotificationTokens.push(snapshot.val().token);
              }
            }
          });
          new Promise(function(resolve, reject) {
            if (androidNotificationTokens.length) {
              console.log('payload.android.data.route', payload.android.data.route);
              payload.android.data.route = message.landingPage;
              console.log('payload.android.data.route', payload.android.data.route);
              admin.messaging().sendToDevice(androidNotificationTokens, payload.android).then(function(response) {
                console.log('sucessfully sent android notifications');
                resolve();
              }).catch(function(err) {
                console.log(err);
                reject(err);
              });
            } else {
              resolve();
            }
          }).then(function() {
            return new Promise(function(resolve, reject) {
              if (iosNotificationTokens.length) {
                admin.messaging().sendToDevice(iosNotificationTokens, payload.ios).then(function(response) {
                  resolve();
                }).catch(function(err) {
                  reject(err);
                });
              } else {
                resolve();
              }
            });
          }).then(function() {
            console.log('IOS notifications sent sucessfully');
            return saveNotification(payload.android.data);
          }).then(function() {
            res.send({
              msg: 'sent sucessfully'
            });
          }).catch(function(err) {
            console.log('Error sending notifications', err);
            res.send({
              msg: 'error sending notifications ' + err
            });
          });

          if (androidNotificationTokens.length < 1 && iosNotificationTokens.length < 1) {
            saveNotification(payload.android.data).then(function(response) {
              console.log('message saved sucessfully', response);
              res.send({
                msg: 'sent sucessfully'
              });
            }).catch(function(err) {
              console.log('err occured', err);
              res.send({
                msg: 'failed to send notifications'
              });
            });
          }
        }).catch(function(error) {
          console.log(error);
          res.send({
            msg: 'error getting tokens ' + error
          });
        });
      } else if (message.target == 'single device') {
        var tokenRef = db.ref('/notificationTokens/' + message.target);
        tokenRef.once('value', function(snapshot) {
          if (snapshot.val() && snapshot.val().token) {
            admin.messaging.sendToDevice(snapshot.val().token, payload.android).then(function() {
              console.log('notification has been sent');
            }).catch(function(err) {
              console.log('error occured', err);
            });
          }
        });
      } else {
        var promises_array = [],
          userIds = [];
        if (message.topics) {
          message.topics.forEach(function(topic) {
            promises_array.push(new Promise(function(resolve, reject) {
              db.ref('/topics/' + topic.eventId + '/subscribers').once('value', function(snapshot) {
                var obj = snapshot.val();
                if (obj) {
                  var keys = Object.keys(obj);
                  userIds = userIds.concat(keys);
                  resolve();
                } else {
                  resolve();
                }
              });
            }));
          });
        } else if (message.userIds) {
          userIds = message.userIds;
        }
        Promise.all(promises_array).then(function() {
          var promises_array2 = [];
          console.log('userIds', userIds);
          userIds = _.uniq(userIds);
          userIds.forEach(function(userId) {
            promises_array2.push(new Promise(function(resolve, reject) {
              db.ref('notificationTokens/' + userId).once('value', function(snapshot) {
                if (snapshot.val() && snapshot.val().token) {
                  if (snapshot.val().type == 'android') {
                    androidNotificationTokens.push(snapshot.val().token);
                  } else {
                    iosNotificationTokens.push(snapshot.val().token);
                  }
                }
                resolve();
              });
            }));
          });
          Promise.all(promises_array2).then(function() {
            console.log('androidNotificationTokens', androidNotificationTokens);
            console.log('iosNotificationTokens', iosNotificationTokens);
            new Promise(function(resolve, reject) {
              if (androidNotificationTokens.length) {
                admin.messaging().sendToDevice(androidNotificationTokens, payload.android).then(function(response) {
                  console.log('notifications sent sucessfully', response);
                  resolve();
                }).catch(function(err) {
                  console.log('err occured', err);
                  reject(err);
                });
              } else {
                resolve();
              }
            }).then(function() {
              return new Promise(function(resolve, reject) {
                if (iosNotificationTokens.length) {
                  console.log('inside iosNotificationTokens.length if', iosNotificationTokens);
                  admin.messaging().sendToDevice(iosNotificationTokens, payload.ios).then(function(response) {
                    console.log('notifications sent sucessfully', response);
                    resolve();
                  }).catch(function(err) {
                    reject(err);
                  });
                } else {
                  resolve();
                }
              }).then(function() {
                return saveNotification(payload.android.data, 'SAVE_FOR_SUBSCRIBED', userIds);
              }).then(function() {
                res.send({
                  msg: 'sent sucessfully'
                });
              }).catch(function(err) {
                console.log('err occured', err);
                res.send({
                  msg: 'failed to send notifications'
                });
              });
            }).catch(function(err) {
              console.log('err occured', err);
              res.send({
                msg: 'failed to send notifications'
              });
            });
            if (androidNotificationTokens.length < 1 && iosNotificationTokens.length < 1) {
              saveNotification(payload.android.data, null).then(function(response) {
                console.log('message saved sucessfully', response);
                res.send({
                  msg: 'sent sucessfully'
                });
              }).catch(function(err) {
                console.log('err occured', err);
                res.send({
                  msg: 'failed to send notifications'
                });
              });
            }
          });
        }).catch(function(err) {
          console.log('err occured', err);
          res.send({
            msg: 'failed to send notifications'
          });
        });
      }
    });
  });

  var saveNotification = function(notification, type, userIds) {
    console.log('save notification ', notification);
    return new Promise(function(resolve, reject) {
      if (notification.editId !== 'newnotification') {
        db.ref('/eventNotifications/' + notification.editId).set(notification).then(function() {
          resolve();
        });
      } else {
        var notificationChildRef = db.ref('/eventNotifications').push();
        notificationChildRef.once('value', function(snapshots) {
          notification.id = notificationChildRef.key;
          console.log(userIds);
          notification.targetUsers = userIds || "ALL_DEVICES";
          console.log('before set ', notification);
          notificationChildRef.set(notification).then(function() {
            resolve();
          });
        });
      }
    });
  };

  exports.saveUserNotifications = functions.database.ref('/eventNotifications/{notificationId}').onCreate(function(event) {
    var notification;
    var promisesArray = [];
    var notificationRef = db.ref('/eventNotifications/' + event.params.notificationId);
    notificationRef.once('value', function(notificationSnapshot) {
      db.ref('/users/').once('value', function(userSnapshot) {
        notification = notificationSnapshot.val();
        var users = userSnapshot.val();
        if (notification.targetUsers != 'ALL_DEVICES' && notification.targetUsers && notification.targetUsers.length > 0) {
          notification.targetUsers.forEach(function(userId) {
            promisesArray.push(new Promise(function(resolve, reject) {
              var userRef = db.ref('/users/' + userId + '/notifications/' + event.params.notificationId).set(true).then(function() {
                resolve();
              }).catch(function(error) {
                reject(error);
              });
            }));
          });
          Promise.all(promisesArray).then(function() {
            db.ref('/eventNotifications/' + event.params.notificationId + '/targetUsers').remove();
          }).catch(function(error) {
            console.log("ERROR :", error);
            db.ref('/eventNotifications/' + event.params.notificationId + '/targetUsers').remove();
          });
        }
      });
    });
  });

  exports.sendEventEmail = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var promise_Array = [],
        usersArray = req.body.usersArray,
        params = req.body.data;
      usersArray.forEach(function(user) {
        var msg = {
          subject: '[Inqude University Admin App] ' + req.body.data.subject,
          html: `<h3 style="font-weight:normal"> Dear ${user.name}, <br><br> ${params.body} </h3>
          <div style="border:1px solid grey; width:450px ">
            <div style="background-color:#ddd; padding: 6px 8px; border-bottom: 1px solid grey; font-weight:bold; font-size:16px;">
              Event Details and Information
            </div>
            <div style="padding: 4px 8px ;font-size:15px">
              Event Name: ${params.event.eventName} <br> Start Time: ${moment(params.event.startDate).format('dddd, MMMM Do, YYYY, hh:mm: A')} <br> End Time: ${moment(params.event.endDate).format('dddd, MMMM Do, YYYY, hh:mm: A')}
            </div>`
        };
        if (params.event.venue || params.event.city) {
          msg.html += `<div style="padding: 4px 8px;font-size:15px;border-top:1px solid grey;"> Address: </div>
          <div style="padding: 4px 8px;font-size:15px"> ${params.event.venue || '' } <br> ${params.event.city || ''} </div>
        </div>`
        } else {
          msg.html += `</div>`;
        }
        promise_Array.push(sendEmail(user.mail, msg));
      });
      Promise.all(promise_Array).then(function() {
        res.send('All Emails are sent');
        console.log("All Emails are sent");
      }).catch(function(error) {
        console.log("Mailing Failed : ", error);
        res.send('Mailing Failed');
      });
    });
  });

  exports.createCustomToken = functions.https.onRequest(function(req, res) {
    cors(req, res, function() { 
      var email = req.body.email,
        uid = req.body.uid,
        isNewUser = true,
        userRef,
        isUserExists = false;
      var user = {};
      db.ref('/users').orderByChild('email')
        .equalTo(req.body.email)
        .once('value')
        .then(function(snapshot) {
          console.log('snapshot.val()', snapshot.val());
          if (snapshot.val()) {
            console.log('inside the snapshot.val()');
            console.log('_.keys(snapshot.val())', _.keys(snapshot.val()));
            (_.keys(snapshot.val())).forEach(function(key) {
              console.log('key', key);
              console.log('((snapshot.val())[key]).appName', ((snapshot.val())[key]).appName);
              console.log(((snapshot.val())[key]).appName == "EVENT_APP");
              if (((snapshot.val())[key]).appName == "EVENT_APP") {
                console.log('inside the if case');
                isUserExists = true;
                isNewUser = false;
              }
            });
          }
          console.log('isUserExists before the if condition', isUserExists);
          if (isUserExists) {
            userRef = db.ref('/users/' + uid);
            console.log(' if else !uid', uid);
            createToken(uid)
              .then(function(customToken) {
                userRef.once('value', function(snapshot) {
                  console.log('user exists', snapshot.exists());
                  isNewUser = false;
                  user = snapshot.val();
                  user.isNewUser = isNewUser;
                  res.send({
                    token: (customToken.toString()),
                    user: user
                  });
                  console.log('custom token is ', (customToken.toString()));
                });
              }).catch(function(error) {
                console.log("Error creating custom token:", error);
              });
          } else {
            userRef = db.ref('/users').push({});
            uid = userRef.key;
            console.log('in else part', userRef.key);
            createToken(uid)
              .then(function(customToken) {
                console.log('created a custom token', customToken);
                userRef.set({
                  uid: uid,
                  roleValue: 0,
                  email: email,
                  appName: 'EVENT_APP'
                }).then(function() {
                  userRef.once('value', function(snapshot) {
                    user = snapshot.val();
                    user.isNewUser = isNewUser;
                    console.log(snapshot.val(), 'user in ref', user);
                    res.send({
                      token: (customToken.toString()),
                      user: user
                    });
                  });
                });
              })
              .catch(function(error) {
                console.log("Error creating custom token:", error);
              });
          }
        });
    });
  });

  var createToken = function(uid) {
    return admin.auth().createCustomToken(uid);
  };

  exports.updateEmail = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var uid = req.body.uid,
        email = req.body.email;
      admin.auth().updateUser(uid, {
        email: email
      }).then(function(userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log("Successfully updated user", userRecord.toJSON());
        db.ref('users/' + uid).once('value', function(snapshot) {
          var user = snapshot.val();
          user.email = email;
          db.ref('users/' + uid).set(user).then(function() {
            console.log();
            res.send({
              msg: 'success'
            });
          }).catch(function(err) {
            console.log("Error updating user:", err);
            res.send({
              msg: 'failed while updating user'
            });
          });
        });
      }).catch(function(err) {
        console.log("Error updating user:", err);
        res.send({
          msg: 'failed while updating user'
        });
      });
    });
  });

  function flushdata(uid, type) {
    return new Promise(function(resolve, reject) {
      var promise_Array = [];
      db.ref('users/' + uid + '/participationStatus')
        .once('value', function(snapshot) {
          (_.keys(snapshot.val())).forEach(function(key) {
            promise_Array.push(new Promise(function(resolve, reject) {
              db.ref('/participants/' + key + '/' + uid)
                .remove()
                .then(function() {
                  resolve();
                })
                .catch(function(err) {
                  resolve();
                });
            }));
          });
          Promise.all(promise_Array)
            .then(function() {
              return db.ref('users/' + uid + '/participationStatus/')
                .remove();
            })
            .then(function() {
              resolve();
            })
            .catch(function(err) {
              console.log('err occured', err);
            });
        });

      // db.ref('users/' + uid + '/notifications')
      //   .remove()
      //   .then(function() {
      //     return db.ref('participants/')
      //       .orderByKey(uid)
      //       .once('value', function(snapshot) {
      //         console.log(snapshot);
      //       })
      //
      //       .then(function() {
      //         resolve();
      //       })
      //       .catch(function(err) {
      //         console.log('err occured', err);
      //         reject(err);
      //       });
      //   })
      //   .catch(function(err) {
      //     console.log('err occured', err);
      //     reject(err);
      //   });
    });
  }
  // function flushdata(uid, roleValue, type) {
  //   return new Promise(function(resolve, reject) {
  //     if (roleValue == 20) {
  //       db.ref('moderatorEvents/' + uid)
  //         .remove();
  //     }
  //     db.ref('moderatorEvents/' + uid)
  //       .remove()
  //       .then(function() {
  //         if (type !== "user") {
  //           return db.ref('users/' + uid + 'notifications')
  //             .remove()
  //             .then(function() {
  //               resolve();
  //             })
  //             .catch(function(err) {
  //               console.log('err occured', err);
  //               reject(err);
  //             });
  //         } else {
  //           resolve();
  //         }
  //       })
  //       .catch(function(err) {
  //         console.log('err occured', err);
  //         reject(err);
  //       });
  //   });
  // }

  exports.flushUser = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var uid = req.body.uid;
      admin.auth().deleteUser(uid)
        .then(function() {
          // See the UserRecord reference doc for the contents of userRecord.
          console.log("Successfully deleted user", uid);
          return db.ref('users/' + uid).remove();
        })
        .then(function() {
          return db.ref('notificationTokens/' + uid)
            .remove();
        })
        .then(function() {
          return flushdata(uid, 'user');
        })
        .then(function() {
          res.send({
            msg: 'success'
          });
        }).catch(function(err) {
          console.log("Error updating user:", err);
          res.send({
            msg: 'failed while updating user'
          });
        });
    });
  });

  exports.flushUserData = functions.https.onRequest(function(req, res) {
    cors(req, res, function() {
      var uid = req.body.uid;
      flushdata(uid, 'userData')
        .then(function() {
          res.send({
            msg: 'success'
          });
        })
        .catch(function(err) {
          console.log('err occured', err);
          res.send({
            msg: 'failed deletung user data'
          });
        });
    });
  });

  // exports.updateEventCounts = functions.database.ref('/participants/{eventId}/{uid}').onDelete(function(event) {
  //    return new Promise(function(resolve, reject){
  //     db.ref('')
  //   });
  // });

})();
