import { Component, ViewEncapsulation } from '@angular/core';
import { ActionSheetController, AlertController, NavController, NavParams, LoadingController, Platform, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import moment from 'moment';

@Component({
  selector: 'custom-page',
  templateUrl: 'custom-page.html',
  encapsulation: ViewEncapsulation.None,

})

export class CustomPage {
  page: any = {};
  pageId: string;
  showBackButton: boolean = false;

  constructor(
    public params: NavParams,
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public helperService: HelperService
  ) {
    this.showBackButton = this.params.get("showBackButton");
    let loading = this.loadingCtrl.create();
    loading.present();
    this.pageId = params.get('pageId');
    afoDatabase.object('/pages/' + this.pageId)
      .subscribe((page) => {
        if (page.title) {
          this.page = page;
        }
        loading.dismiss();
      });
  }

  formatDate(date: any) {
    return moment.utc(date).format('lll');
  }

}
