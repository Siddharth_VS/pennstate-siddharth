import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Talisma } from '../../providers/crm/talisma';
import { HelperService } from '../../providers/helperService';
import { UserData } from '../../providers/user-data';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import * as _ from 'underscore';

@Component({
  selector: 'speaker-detail',
  templateUrl: 'speaker-detail.html'
})

export class SpeakerDetail {
  speaker: any;
  eventId: any;
  participationStatus: any = true;
  allEvents:any=[];
  speakerEventIds:any=[];
  uid: any;
  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public navParams: NavParams,
    public talisma: Talisma,
    public userData: UserData
  ) {
    this.userData.getUid().
      then((uid) => {
        this.participationStatus = navParams.get('participationStatus');
        this.speaker = navParams.get('speaker');
        this.uid = uid;
        this.helperService.showLoading();
        this.getSpeakerEvents(this.speaker);
      })
  }

  getSpeakerEvents(speaker: any) {
    this.speakerEventIds = _.sortBy(speaker.Events, function(eventId) { return eventId });
    this.afoDatabase.object('/events/')
      .take(1)
      .subscribe((allEvents: any) => {
        this.allEvents=allEvents;
        this.helperService.hideLoading();
      })
  }
}
