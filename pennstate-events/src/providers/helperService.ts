import { Injectable } from "@angular/core";
import { App, Platform, ToastController, LoadingController } from "ionic-angular";
import { AngularFire } from "angularfire2";
import { AngularFireOfflineDatabase } from "angularfire2-offline/database";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import { Settings } from "../constants/firebase-config";
import { CookieService } from "ngx-cookie";
import { AlertController } from "ionic-angular";
import { EmailComposer } from "@ionic-native/email-composer";
import { Storage } from "@ionic/storage";
import * as moment from "moment-timezone";
import * as _ from "underscore";

@Injectable()
export class HelperService {
  loaderInstance: any = null;
  timeZone: any = "America/Matamoros";
  participationStatus: any = {
    1: "Invited",
    2: "Registered",
    3: "Attended",
    4: "Canceled",
    5: "CancellationRequested",
    6: "Interested",
    7: "InterestedInFutureEvents",
    8: "Pending",
    9: "Confirmed",
    10: "Approved",
    11: "Declined",
    12: "FollowUp",
    13: "NoShow",
    14: "Transferred",
    15: "Substituted",
    16: "Waitlisted",
    17: "PaymentFailed",
    18: "Deleted",
    19: "WaitlistRTR",
    20: "RegisteredPayLater",
    21: "TransferredPayLater",
    22: "SubstitutedPayLater",
    23: "EventCancelled"
  };

  constructor(
    public alertCtrl: AlertController,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    private cookieService: CookieService,
    public email: EmailComposer,
    public http: Http,
    public platform: Platform,
    public toastCtrl: ToastController,
    public storage: Storage,
    public loadingCtrl: LoadingController
  ) { }

  getUid(): Promise<string> {
    return this.storage.get("uid").then((value) => {
      return value;
    });
  }

  getRoleValue(): Promise<number> {
    return this.storage.get("roleValue").then((value: any) => {
      return value;
    });
  }

  convertFromUtcToTz(datetime: any) {
    datetime = datetime || "";
    return moment.utc(datetime).tz(this.timeZone);
  }

  getTimeStamp() {
    return moment()
      .tz(this.timeZone)
      .subtract(1, "days")
      .endOf("day")
      .format("");
  }

  convertToTz(someEvents: any) {
    return new Promise((resolve) => {
      someEvents.forEach((event: any) => {
        event.EventStartDate = moment(event.EventStartDate).tz(this.timeZone);
      });
      resolve(someEvents);
    });
  }

  // show Loading UI
  showLoading() {
    this.loaderInstance = this.loadingCtrl.create();
    this.loaderInstance.present();
    // return this.loaderInstance;
  }

  formatFileSize(bytes: number) {
    if (bytes < 1024) return bytes + " Bytes";
    else if (bytes < 1048576) return (bytes / 1024).toFixed(1) + " KB";
    else if (bytes < 1073741824) return (bytes / 1048576).toFixed(1) + " MB";
    else return (bytes / 1073741824).toFixed(1) + " GB";
  };

  // hide the Loading UI
  hideLoading() {
    if (this.loaderInstance !== null) {
      this.loaderInstance.dismiss();
      this.loaderInstance = null;
    }
  }

  showMessage(msg: any, duration?: any, className?: any) {
    this.toastCtrl
      .create({
        message: msg,
        duration: duration || 2000,
        cssClass: className || ""
      })
      .present();
  }

  // get InAppBrowser params
  getBrowserSettings() {
    return "location=yes,closebuttoncaption=Done,toolbar=yes,presentationstyle=pagesheet,transitionstyle=fliphorizontal";
  }

  // get participationStatus value
  getStatusValue(value: any) {
    return this.participationStatus[value];
  }

  validateParticipant(data: any) {
    let result = data.split("<!DOCTYPE html>")[0];
    return new Promise((resolve, reject) => {
      if (result && result.includes("Participant Creation successful")) {
        resolve(parseInt(result.split(":")[1]));
      }
      if (result && result.includes("Error occurred")) {
        reject("Participant creation error");
      }
    });
  }

  validateStatusUpdate(data: any) {
    new Promise((resolve, reject) => {
      if (data && data.includes("Participant update successful")) {
        resolve();
      }
      if (data && data.includes("Error occurred while updating")) {
        reject("Participant status update error");
      }
    });
  }

  //generates unique
  generateUUID(): Promise<{ uuid: string }> {
    return new Promise((resolve, reject) => {
      let d = new Date().getTime();
      if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
      }
      let uuid = "xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx".replace(/[xy]/g, function(c) {
        var r = ((d + Math.random() * 16) % 16) | 0;
        d = Math.floor(d / 16);
        return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
      });
      resolve({ uuid: uuid });
    });
  }

  getHeaders() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Access-Control-Allow-Origin", "*");
    return headers;
  }

  isAdmin() {
    return new Promise((resolve, reject) => {
      this.getRoleValue().then((roleValue: any) => {
        if (roleValue == 99) {
          resolve();
        } else {
          this.alertCtrl
            .create({
              title: "Warning",
              subTitle: "You're not authorized to perform the selected operation",
              buttons: ["Dismiss"]
            })
            .present();
          reject("Not enough privileges to perform action");
        }
      });
    });
  }

  sendOtp(email: string, type?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getHeaders();
      let headers = this.getHeaders();
      let body: any = JSON.stringify({ email: email.toLowerCase() });
      type ? (body["type"] = type) : "";
      this.afoDatabase
        .object("settings/emailProxy")
        .take(1)
        .subscribe((emailProxy: any) => {
          if (emailProxy.isEnabled && emailProxy.email == email.toLowerCase()) {
            resolve({ msg: "sucess", otp: emailProxy.otp });
          } else {
            this.http
              .post(Settings.cloudFunctionURL + "/sendOtp", body, headers)
              .map((res: any) => res.json())
              .subscribe((data) => {
                if (data && data.message && data.message.includes("error")) {
                  reject(data.message);
                } else {
                  resolve({ msg: data.message, otp: data.otp });
                }
              });
          }
        });
    });
  }

  sendEmailTicket(data: any) {
    let headers = this.getHeaders();
    let body = JSON.stringify({ data: data });
    this.http.post(Settings.cloudFunctionURL + '/sendEmailTicket', body, { headers: headers })
      .subscribe((res) => {
        console.log(res)
      })
  }

  createToken(uid: string, email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ email: email, uid: uid });
      this.http
        .post(Settings.cloudFunctionURL + "/createCustomToken", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  resetPassword(email: string, password: string): Promise<{ msg: any }> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let cred = { email: email.toLowerCase(), password: password };
      let body = JSON.stringify(cred);
      this.http
        .post(Settings.cloudFunctionURL + "/resetPassword", body, headers)
        .map((res: any) => res.json())
        .subscribe((data: any) => {
          if (data && data.message == "success") {
            resolve(data);
          } else {
            reject(data);
          }
        });
    });
  }

  createUser(user: any): Promise<string> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify(user);
      this.http
        .post(Settings.cloudFunctionURL + "/createUser", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res: any) => {
          if (res && res.message == "success") {
            resolve(res);
          } else {
            console.log("reject user creation ", res);
            reject(res);
          }
        });
    });
  }

  isUserExists(email: string): Promise<any> {
    return new Promise((resolve) => {
      let users: any = [];
      this.afoDatabase.list("users", { query: { orderByChild: "email", equalTo: email.toLowerCase() } }).subscribe((snapshots: any) => {
        if (snapshots && snapshots.length) {
          _.each(snapshots, (snapshot: any) => {
            if (snapshot.appName && snapshot.appName == "EVENT_APP") {
              delete snapshot.$exists;
              delete snapshot.$key;
              snapshot.isUserExists = true;
              users.push(snapshot);
            }
          });
        }
        console.log('users', users)
        resolve(users);
      });
    });
  }

  sendNotification(message: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ message: message });
      this.http
        .post(Settings.cloudFunctionURL + "/sendNotifications", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  flushUserData(uid: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ uid: uid });
      this.http
        .post(Settings.cloudFunctionURL + "/flushUserData", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  subscribeToEventNotifications(session: any, uid: string) {
    return new Promise((resolve, reject) => {
      this.af.database
        .object("topics/" + session.id + "/subscribers/" + uid)
        .set(true)
        .then(() => {
          this.af.database
            .list("/notificationTargets", { query: { orderByChild: "eventId", equalTo: session.id } })
            .take(1)
            .subscribe((res) => {
              if (!res.length) {
                this.af.database
                  .list("notificationTargets")
                  .push({})
                  .set({ name: session.name, eventId: session.id })
                  .then(() => {
                    resolve("sucess");
                  })
                  .catch((err) => {
                    console.log("error occured", err);
                    reject(err);
                  });
              } else {
                resolve("sucess");
              }
            });
        })
        .catch((err) => {
          console.log("error occured", err);
          reject(err);
        });
    });
  }

  unSubscribeToEventNotifications(session: any, uid: string) {
    return new Promise((resolve, reject) => {
      this.af.database
        .object("topics/" + session.id + "/subscribers/" + uid)
        .remove()
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
          console.log("error occured", err);
        });
    });
  }

  createCustomToken(uid: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ uid: uid });
      this.http
        .post(Settings.cloudFunctionURL + "/createCustomToken", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          if (res.msg && res.msg.includes("sucess")) {
            resolve(res);
          } else {
            reject(res);
          }
        });
    });
  }
}
