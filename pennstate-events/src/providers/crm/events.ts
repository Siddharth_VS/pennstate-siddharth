import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';

@Injectable()
export class Events extends BaseClass {
  constructor(
    public apiService: ApiService
  ) {
    super(apiService);
    this.key = 'EventId';
    this.url = 'Events'
  }

  getEvents(number: number, startPoint?: number, filter?: string) {
    let subURl = this.url + '?' + (filter ? (filter + '&') : '') + '$skip=' + (startPoint ? startPoint : 0) + '&$top=' + number;
    return this.apiService.ajax(this.apiService.getUrl(subURl), 'object')
  }
  // get speakers for an specific event
  getSpeakers(EventId: any) {
    let subURl = this.url + '?$select=Speakersforthisevent&$filter=' + this.key + ' eq ' + EventId + '&$expand=Speakersforthisevent($select=Name,Email,ContactId)';
    return this.apiService.ajax(this.apiService.getUrl(subURl), 'object');
  }

  getEventById(query: any) {
    return super.find(query);
  }

  getFilteredEvents(subUrl: any) {
    let url = this.url + '?$filter=' + subUrl;
    return this.apiService.ajax(this.apiService.getUrl(url), 'object')
  }

}
