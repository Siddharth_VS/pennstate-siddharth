import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';

@Injectable()
export class Contacts extends BaseClass {
  constructor(
    public apiService: ApiService,
    public base: BaseClass
  ) {
    super(apiService);
    this.key = 'ContactId';
    this.url = 'Contacts'
  }

  getUserEventIds(contactId: string, userFields: any[], eventFields: any[], filter?: string) {
    return new Promise((resolve, reject) => {
      filter = filter ? filter : '';
      let query = 'ContactId eq ' + contactId + '&$select=' + userFields.join(',') + '&$expand=Eventsforthisspeaker($select=' + eventFields.join(',') + filter + ')';
      super.find(query)
        .then((response: any) => {
          resolve(response);
        })
        .catch((err) => {
          console.log('err occured', err);
          reject(err)
        });
    });
  }

  // get speaker details and events associated with this speaker
  getSpeakersDetails(contactId: any, contactParmas: any, speakerParams: any) {
    let subURl = this.url + '?$select=' + contactParmas + '&$filter=' + this.key + ' eq ' + contactId + '&$expand=Eventsforthisspeaker($select=' + speakerParams + ')';
    return this.apiService.ajax(this.apiService.getUrl(subURl), 'object');
  }
}
