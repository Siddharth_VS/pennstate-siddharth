import { Injectable } from '@angular/core';
import { App } from 'ionic-angular/index';
import { InAppBrowser } from 'ionic-native';
import { CustomPage } from '../pages/custom-page/custom-page';
import { EventListPage } from '../pages/event-list/event-list';
import { CalenderEventListPage } from '../pages/event-list-new/event-list-new';
import { LoginPage } from '../pages/login/login';
import { MyEventsPage } from '../pages/my-events/my-events';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { EditModerator } from '../components/moderator/edit-moderator';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { Events } from '../providers/crm/events';
import { HelperService } from '../providers/helperService';

@Injectable()
export class LinkService {
  components: any = {};

  constructor(
    public app: App,
    public events: Events,
    public helperService: HelperService
  ) {
    this.components = {
      'MyEventsPage': MyEventsPage,
      'EditModerator': EditModerator,
      'EventDetailPage': EventDetailPage,
      'EventListPage': EventListPage,
      'NavigationDashboardPage': NavigationDashboardPage,
      'LoginPage': LoginPage,
      'CalenderEventListPage':CalenderEventListPage
    }
  }

  openBrowser(link: any) {
    if (link.indexOf('http') < 0) {
      link = 'http://' + link;
    }
    new InAppBrowser(link, '_blank', 'location=yes,closebuttoncaption=Done,toolbar=yes,presentationstyle=pagesheet,transitionstyle=fliphorizontal');
  }

  viewPage(menu: any) {
    // notification data will be in string > convert to object
    if (menu && typeof (menu) == 'string') {
      menu = JSON.parse(menu);
    }
    if (menu.type == 'COMPONENT') {
      this.app.getRootNav().setRoot(this.components[menu.value], { title: menu.title });
    } else if (menu.type == 'LAUNCH_URL') {
      this.openBrowser(menu.value);
    } else if (menu.type == 'PAGE') {
      this.app.getRootNav().setRoot(CustomPage, { pageId: menu.value, title: menu.title });
    } else if (menu.type == 'SCHEDULE') {
      this.helperService.showLoading();
      this.getEventDetails(menu.value)
        .then((eventData) => {
          this.helperService.hideLoading();
          this.app.getRootNav().setPages([{ page: EventListPage }, { page: EventDetailPage, params: { event: eventData } }]);
        })
        .catch(error => {
          console.log(error);
        })
    }
  }

  getEventDetails(eventId: any) {
    return new Promise((resolve, reject) => {
      let event = {};
      this.events.getEventById("(EventId eq " + eventId + ")")
        .then((res: any) => {
          event = res.length ? res[0] : {}
          event ? resolve(event) : reject("Not Found");
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    })
  };
}
