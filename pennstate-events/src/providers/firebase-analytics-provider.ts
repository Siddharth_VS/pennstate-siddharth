import { Injectable } from '@angular/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { Platform } from 'ionic-angular';

@Injectable()

export class FirebaseAnalyticsProvider {
  constructor(
    private firebaseAnalytics: FirebaseAnalytics,
    public platform: Platform
  ) { }
  checkPlatform() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android') || this.platform.is('ios')) {
        resolve();
      }
    })
  }
  logEvent(name: string, value: any) {
    this.checkPlatform().then(() => {
      return this.firebaseAnalytics.logEvent(name, value)
    })
  }

  setUser(properties: any) {
    this.checkPlatform().then(() => {
      for (let key of (<any>Object).keys(properties)) {
        this.firebaseAnalytics.setUserProperty(key, properties[key])
      }
    })
  }

  setUserId(id: string) {
    this.checkPlatform().then(() => {
      this.firebaseAnalytics.setUserId(id)
    })
  }

  setScreen(name: string) {
    this.checkPlatform().then(() => {
      this.firebaseAnalytics.setCurrentScreen(name)
    })
  }

  // logError(message: any) {
  //   this.checkPlatform().then(() => {
  //     this.firebaseAnalytics.logError(message)
  //   })
  // }

}
