import { Injectable, Inject } from "@angular/core";
import { Platform, App } from "ionic-angular";
import { AngularFire } from "angularfire2";
import { AngularFireOfflineDatabase } from "angularfire2-offline/database";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import { Settings } from "../constants/firebase-config";
import { CookieService } from "ngx-cookie";
import * as moment from "moment-timezone";
import * as _ from "underscore";
import { UserData } from "../providers/user-data";
import { AlertController } from "ionic-angular";
import { LaunchNavigator } from "@ionic-native/launch-navigator";
import { InAppBrowser } from "ionic-native";

@Injectable()
export class HelperService {
  url: any;
  JSON: JSON;
  timeZone: any;
  pushObject: any;

  constructor(
    @Inject(UserData) public userData: UserData,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public app: App,
    private cookieService: CookieService,
    public http: Http,
    private launchNavigator: LaunchNavigator,
    public platform: Platform
  ) {
    this.JSON = JSON;

    this.afoDatabase
      .object("settings/timeZone")
      .take(1)
      .subscribe((zone: any) => {
        this.timeZone = zone.$value;
      });
  }

  //generates unique
  generateUUID(): Promise<{ uuid: string }> {
    return new Promise((resolve, reject) => {
      let d = new Date().getTime();
      if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
      }
      let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
        var r = ((d + Math.random() * 16) % 16) | 0;
        d = Math.floor(d / 16);
        return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
      });
      resolve({ uuid: uuid });
    });
  }

  convertFromUtcToTz(datetime: any) {
    datetime = datetime || "";
    return moment.utc(datetime).tz(this.timeZone);
  }

  getCurrentTimeFromTz() {
    return moment()
      .tz(this.timeZone)
      .format("");
  }

  getTimeTzFormat(format: string) {
    if (format === "MMMM") {
      return moment
        .tz(this.timeZone)
        .set({ hour: 0, minute: 0, second: 0, date: 1 })
        .format(format ? format : "");
    }
    return moment.tz(this.timeZone).format(format ? format : "");
  }

  isSessionOver(session: any) {
    let currentTime = this.getCurrentTimeFromTz();
    let flag: boolean = false;

    if (session.startTime && session.startTime._isAMomentObject && currentTime > session.endTime.format("")) {
      flag = true;
    }
    return flag;
  }

  isSessionLive(session: any) {
    let currentTime = this.getCurrentTimeFromTz();
    let flag: boolean = false;
    if (
      session.startTime &&
      session.startTime._isAMomentObject &&
      session.endTime._isAMomentObject &&
      currentTime >= session.startTime.format("") &&
      currentTime <= session.endTime.format("")
    ) {
      flag = true;
    }
    return flag;
  }

  setTimeInTz(segment: string) {
    let segmentParts: any = segment.split("/");
    let params: any = {
      year: segmentParts[2],
      month: parseInt(segmentParts[0]) - 1,
      date: segmentParts[1],
      hour: 0,
      minute: 0
    };

    return moment()
      .tz(this.timeZone)
      .set(params);
  }

  defaultImageUrl(): Promise<{ url: string }> {
    return new Promise((resolve, reject) => {
      let item = this.af.database.object("/images", { preserveSnapshot: true });
      item.subscribe((snapshot) => {
        this.url = snapshot.val().defaultImage;
        resolve({ url: this.url });
      });
    });
  }

  getHeaders() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Access-Control-Allow-Origin", "*");
    return headers;
  }

  sendOtp(email: string, type?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getHeaders();
      let headers = this.getHeaders();
      let data: any = { email: email.toLowerCase(), appType: "EVENT_APP" };
      type ? (data["type"] = type) : "";
      let body: any = JSON.stringify(data);
      this.afoDatabase
        .object("settings/emailProxy")
        .take(1)
        .subscribe((emailProxy: any) => {
          if (emailProxy.isEnabled && emailProxy.email == email.toLowerCase()) {
            resolve({ msg: "sucess", otp: emailProxy.otp });
          } else {
            this.http
              .post(Settings.cloudFunctionURL + "/sendOtp", body, headers)
              .map((res: any) => res.json())
              .subscribe((data) => {
                if (data && data.message && data.message.includes("error")) {
                  reject(data.message);
                } else {
                  resolve({ msg: data.message, otp: data.otp });
                }
              });
          }
        });
    });
  }

  resetPassword(email: string, password: string): Promise<{ msg: any }> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let cred = { email: email.toLowerCase(), password: password };
      let body = JSON.stringify(cred);
      this.http
        .post(Settings.cloudFunctionURL + "/resetPassword", body, headers)
        .map((res: any) => res.json())
        .subscribe((msg) => {
          resolve({ msg: msg });
        });
    });
  }

  createUser(user: any): Promise<string> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify(user);
      this.http
        .post(Settings.cloudFunctionURL + "/createUser", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  manageUser(user: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let cred = { uid: user.uid, disabled: user.disabled };
      let body = this.JSON.stringify(cred);
      this.http
        .post(Settings.cloudFunctionURL + "/manageUser", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  deleteUser(uid: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let cred = { uid: uid };
      let body = this.JSON.stringify(cred);
      this.http
        .post(Settings.cloudFunctionURL + "/deleteUser", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  sendAccountConfirmationMail(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = this.JSON.stringify({ email: email });
      this.http
        .post(Settings.cloudFunctionURL + "/accountConfirmationMail", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  isUserExists(email: string): Promise<any> {
    return new Promise((resolve) => {
      let users: any = [];
      this.afoDatabase.list("users", { query: { orderByChild: "email", equalTo: email.toLowerCase() } }).subscribe((snapshots: any) => {
        if (snapshots && snapshots.length) {
          _.each(snapshots, (snapshot: any) => {
            if (snapshot.appName && snapshot.appName == "EVENT_APP") {
              delete snapshot.$exists;
              delete snapshot.$key;
              snapshot.isUserExists = true;
              users.push(snapshot);
            }
          });
        }
        resolve(users);
      });
    });
  }

  formatFileSize(bytes: number) {
    if (bytes < 1024) return bytes + " Bytes";
    else if (bytes < 1048576) return (bytes / 1024).toFixed(1) + " KB";
    else if (bytes < 1073741824) return (bytes / 1048576).toFixed(1) + " MB";
    else return (bytes / 1073741824).toFixed(1) + " GB";
  }

  sendNotification(message: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ message: message });
      this.http
        .post(Settings.cloudFunctionURL + "/sendNotifications", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  sendNexmoOtp(phone: any): Promise<any> {
    // to enable Proxy-SMS-gateway
    // set cookie 'PROXY_SMS_GATEWAY = true'
    if (this.cookieService.get("PROXY_SMS_GATEWAY") || phone.toString() === "911111100000") {
      return new Promise((resolve, reject) => {
        console.log("PROXY-OTP-SMS: 2017");
        resolve({
          status: 0,
          request_id: "PROXY_SMS_GATEWAY_2017"
        });
      });
    }

    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ phone: phone });
      this.http
        .post(Settings.cloudFunctionURL + "/nexmoSendOtp", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  verifyNexmoOtp(requestId: string, otp: string, phone: any): Promise<any> {
    // to enable Proxy-SMS-gateway
    // set cookie 'PROXY_SMS_GATEWAY = true'
    if (this.cookieService.get("PROXY_SMS_GATEWAY") || phone.toString() === "911111100000") {
      return new Promise((resolve, reject) => {
        if (parseInt(otp) == 2017) {
          resolve({ status: 0 });
        } else {
          resolve({
            status: 1,
            message: "OTP didn't match"
          });
        }
      });
    }

    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ requestId: requestId, otp: otp });
      this.http
        .post(Settings.cloudFunctionURL + "/nexmoVerfiyOtp", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  createToken(uid: string, email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ email: email, uid: uid });
      this.http
        .post(Settings.cloudFunctionURL + "/createCustomToken", body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  createShortLink(imageUrl: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (imageUrl.includes("/goo.gl/")) {
        resolve({ shortenUrl: imageUrl });
      } else {
        let headers = this.getHeaders();
        let body = JSON.stringify({ imageUrl: imageUrl });
        this.http
          .post(Settings.cloudFunctionURL + "/shortenUrl", body, { headers: headers })
          .map((res: any) => res.json())
          .subscribe((res) => {
            resolve(res);
          });
      }
    });
  }

  getTabs() {
    return new Promise((resolve, reject) => {
      this.afoDatabase.list("/menus/TAB_MENU/", { preserveSnapshot: true }).subscribe((tabMenus) => {
        resolve(tabMenus !== null ? tabMenus : []);
      });
    });
  }

  isSubscribed(sessionName: string, uid: string) {
    return new Promise((resolve, reject) => {
      this.af.database
        .object("topics/" + sessionName + "/subscribers/" + uid)
        .take(1)
        .subscribe((res) => {
          resolve(res.$value == null ? false : true);
        });
    });
  }

  subscibeToNotifications(sessionId: any, uid: string) {
    return new Promise((resolve, reject) => {
      this.afoDatabase
        .object("schedule/" + sessionId)
        .take(1)
        .subscribe((session) => {
          if (session.$exists()) {
            this.af.database
              .object("topics/" + session.name + "/subscribers/" + uid)
              .set(true)
              .then(() => {
                this.af.database
                  .list("/notificationTargets/", { query: { orderByValue: session.name, equalTo: session.name } })
                  .take(1)
                  .subscribe((res) => {
                    if (res.length === 0) {
                      this.af.database
                        .list("notificationTargets")
                        .push({})
                        .set(session.name)
                        .then(() => {
                          resolve("sucess");
                        })
                        .catch((err) => {
                          console.log("error occured", err);
                          reject(err);
                        });
                    } else {
                      resolve("sucess");
                    }
                  });
              })
              .catch((err) => {
                console.log("error occured", err);
                reject(err);
              });
          }
        });
    });
  }

  unSubscibeToNotifications(sessionId: string, uid: string) {
    return new Promise((resolve, reject) => {
      this.afoDatabase
        .object("schedule/" + sessionId)
        .take(1)
        .subscribe((session) => {
          if (session.$exists()) {
            this.af.database
              .object("topics/" + session.name + "/subscribers/" + uid)
              .remove()
              .then(() => {
                resolve();
              })
              .catch((err) => {
                reject(err);
                console.log("error occured", err);
              });
          }
        });
    });
  }

  sendNotificationForUser(message: any) {
    message.createdDate = moment.utc().format("x");
    message.editId = "newnotification";
    return this.sendNotification(message);
  }

  isAdmin() {
    return new Promise((resolve, reject) => {
      this.userData.getRoleValue().then((roleValue) => {
        if (roleValue == 99) {
          resolve();
        } else {
          this.alertCtrl
            .create({
              title: "Warning",
              subTitle: "You're not authorized to perform the selected operation",
              buttons: ["Dismiss"]
            })
            .present();
          reject("Not enough privileges to perform action");
        }
      });
    });
  }

  openMap(address: any) {
    let params: string;
    if (address.latitude && address.longitude) {
      params = "http://maps.google.com/maps?daddr=" + address.latitude + "," + address.longitude;
    } else {
      address = address.replace(/ /g, "+");
      params = "https://www.google.com/maps/search/" + address;
    }

    if (this.platform.is("ios") || this.platform.is("android")) {
      this.launchNavigator
        .navigate(address.latitude && address.longitude ? [address.latitude, address.longitude] : address)
        .then((success) => console.log("Launched navigator"), (error) => console.log("Error launching navigator", error));
    }
    if (this.platform.is("core")) {
      new InAppBrowser(params, "_blank", "location=yes");
    }
  }
}
