import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UOAEvents } from './app.component';
import { MomentModule } from 'angular2-moment';
import { Settings } from '../constants/firebase-config';
import { Keyboard } from '@ionic-native/keyboard';
import { CodePush } from '@ionic-native/code-push';
import { Device } from '@ionic-native/device';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

import { HelperService } from '../providers/helperService';
import { LinkService } from '../providers/linkService';
import { UserData } from '../providers/user-data';

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { AngularFireOfflineModule } from 'angularfire2-offline';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CookieModule } from 'ngx-cookie';

import { MomentAgo } from "../components/momentAgo/momentAgo";
import { CustomPage } from '../pages/custom-page/custom-page';
import { Push } from '@ionic-native/push';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { QRCodeModule } from 'angular2-qrcode';
import { Calendar } from '@ionic-native/calendar';

// pages
import { EventListPage } from '../pages/event-list/event-list';
import { MyEventsPage } from '../pages/my-events/my-events';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { LoginPage } from '../pages/login/login';
import { SpeakerDetail } from '../pages/speakers/speaker-detail';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { HomePage } from '../pages/home/home';
import { OtpPage } from '../pages/otp/otp';
import { CalenderEventListPage } from '../pages/event-list-new/event-list-new';
import { UserDetailFormPage } from '../pages/users/user-detail-form/user-detail-form';
import { PostModalPage } from '../pages/posts-modal/posts-modal';

// directives
import { ShowForDirective } from '../directives/show-for';
import { Barcode } from '../directives/barcode';
import { ProgressBar } from '../directives/progress-bar';
import { PollProgressBar } from '../directives/poll-progress-bar';
import { BackgroundImage } from '../directives/background-image';
import { ClickActionDirective } from '../directives/click-action';
import { ImageCacheDirective } from '../directives/image-cache';

// components
import { SocialLinksComponent } from '../components/social-links/social-links';
import { ProfileCardComponent } from '../components/profile-card/profile-card';
import { UserCard } from '../components/user-card/user-card';
import { Files } from "../components/files/files"
import { NoResults } from '../components/no-results/no-results';
import { TextAvatarDirective } from '../components/text-avatar/text-avatar.ts';
import { EditModerator } from '../components/moderator/edit-moderator';
import { SpeakerCard } from '../components/speaker-card/speaker-card';
import { SingleEvent } from '../components/single-event/single-event';
import { MomentDiff } from "../components/moment-diff/moment-diff";
import { StaticMap } from "../components/static-map/static-map";
import { NotificationsListPage } from "../pages/notification-list/notification-list";
import { RatingComponent } from '../components/rating/rating';
import { RatingModal } from '../components/rating/rating-modal/rating-modal';
import { Like } from '../components/like/like';
import { EventFilter } from '../components/event-filter/event-filter';
import { EventTicket } from '../components/event-ticket/event-ticket';
import { SingleEventNew } from '../components/single-event-new/single-event-new';
import { FooterTabs } from '../components/footer-tabs/footer-tabs';

//post-module
import { BannerPostComponent } from '../post-module/components/banner/banner-post.component';
import { PollPostComponent } from '../post-module/components/poll/poll-post';
import { PostCardComponent } from '../post-module/post-card.component';
import { BannerWithDescriptionComponent } from '../post-module/components/banner-with-description/banner-with-description-post-component';
import { CalendarPostComponent } from '../post-module/components/calendar/calendar-post-component';
import { PostDirective } from '../post-module/post.directive';
import { PostService } from '../post-module/post.service';
import { YouTubePostComponent } from '../post-module/components/youtube/youtube-post.component';
import { HtmlPostComponent } from '../post-module/components/html/html-post.component';
import { ImageGalleryPostComponent } from '../post-module/components/image-gallery/image-gallery-post.component';

//pipes
import { SearchPipe } from '../pipes/search-filter';
import { YouTube } from '../pipes/youtube.ts';
// import { SafeHtml } from '../pipes/safe-html';

import { UtilProvider } from '../providers/util-provider';

export const firebaseConfig = Settings.firebaseApp;
export const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};
// crm
import { ApiService } from '../providers/crm/apiService';
import { ApiConfigurations } from "../providers/crm/apiConfig";
import { BaseClass } from '../providers/crm/base';
import { Events } from "../providers/crm/events";
import { Contacts } from "../providers/crm/contacts";
import { Participants } from "../providers/crm/participants";
import { Speakers } from "../providers/crm/speakers";
import { Talisma } from "../providers/crm/talisma";

// olli portal configurations
import { PortalConfiguration } from '../constants/portal-config';

@NgModule({
  declarations: [
    StaticMap,
    EventFilter,
    Like,
    YouTube,
    BannerPostComponent,
    PollPostComponent,
    PostCardComponent,
    BannerWithDescriptionComponent,
    CalendarPostComponent,
    PostDirective,
    YouTubePostComponent,
    HtmlPostComponent,
    ImageGalleryPostComponent,
    UserCard,
    Files,
    NoResults,
    UOAEvents,
    EventListPage,
    MyEventsPage,
    EventDetailPage,
    LoginPage,
    ShowForDirective,
    Barcode,
    ProgressBar,
    TextAvatarDirective,
    EditModerator,
    SearchPipe,
    MomentAgo,
    MomentDiff,
    CustomPage,
    PollProgressBar,
    SpeakerCard,
    NotificationsListPage,
    RatingComponent,
    RatingModal,
    SpeakerDetail,
    SingleEvent,
    NavigationDashboardPage,
    HomePage,
    OtpPage,
    UserDetailFormPage,
    CalenderEventListPage,
    BackgroundImage,
    ImageCacheDirective,
    ClickActionDirective,
    PostModalPage,
    EventTicket,
    SingleEventNew,
    SocialLinksComponent,
    ProfileCardComponent,
    FooterTabs,
    // SafeHtml
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CookieModule.forRoot(),
    IonicModule.forRoot(UOAEvents),
    AngularFireModule.initializeApp(firebaseConfig, firebaseAuthConfig),
    AngularFireOfflineModule,
    MomentModule,
    QRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    UserCard,
    Files,
    EventFilter,
    StaticMap,
    UOAEvents,
    EventListPage,
    MyEventsPage,
    EventDetailPage,
    LoginPage,
    EditModerator,
    CustomPage,
    SpeakerCard,
    NotificationsListPage,
    RatingComponent,
    RatingModal,
    SpeakerDetail,
    SingleEvent,
    NavigationDashboardPage,
    HomePage,
    OtpPage,
    UserDetailFormPage,
    BannerPostComponent,
    PollPostComponent,
    PostCardComponent,
    BannerWithDescriptionComponent,
    CalendarPostComponent,
    YouTubePostComponent,
    HtmlPostComponent,
    ImageGalleryPostComponent,
    CalenderEventListPage,
    PostModalPage,
    EventTicket,
    SingleEventNew,
    SocialLinksComponent,
    ProfileCardComponent,
    FooterTabs
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler },
    HelperService,
    PostService,
    LinkService,
    UserData,
    StatusBar,
    SplashScreen,
    Storage,
    FirebaseAnalytics,
    Push,
    EmailComposer,
    Keyboard,
    CodePush,
    Device,
    ApiService,
    ApiConfigurations,
    Events,
    Contacts,
    Participants,
    Speakers,
    Talisma,
    BaseClass,
    UtilProvider,
    InAppBrowser,
    LaunchNavigator,
    PortalConfiguration,
    Calendar
  ]
})
export class AppModule {
}
