import { Component, Input } from '@angular/core';
import { LoadingController, ModalController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { RatingModal } from './rating-modal/rating-modal';
import { HelperService } from '../../providers/helperService';
import { UserData } from '../../providers/user-data';
import * as _ from 'underscore';

@Component({
  selector: 'rating',
  templateUrl: 'rating.html'
})

export class RatingComponent {
  @Input('args') args: any;
  @Input('disabled') isDisabled: any;
  isInitCalled: boolean = false;
  totalRatings = 0;
  rate: any;
  isFeedbackExists: boolean;
  icons: string[] = [];
  username: any = {};
  ratings: any[] = [];
  myFeedback: any;
  totalVotes: any;
  roleValue: any;

  constructor(
    public af: AngularFire,
    public loadingCtrl: LoadingController,
    public modal: ModalController,
    public helperService: HelperService,
    public userData: UserData
  ) {
    this.userData.getUsername()
      .then((username) => {
        this.username = username;
        this.userData.getRoleValue()
          .then((roleValue) => {
            // 0 Added for Testing Purpose Only !
            this.roleValue = roleValue || 0
          })
      });
  }


  ngOnChanges() {
    let self = this;
    if (this.args.uid && this.args.eventId && !this.isInitCalled) {
      this.checkFeedbackExists();
      this.isInitCalled = true;
      this.af.database.object('/feedbacks/' + this.args.eventId, { preserveSnapshot: true })
        .subscribe((session) => {
          let sumOfRatings: number = 0;
          this.totalVotes = 0;
          session = session.val();
          _.each(session, function(val, key) {
            sumOfRatings += session[key].rating;
            self.totalVotes += 1;
          })

          if (this.totalVotes > 0) {
            this.rate = sumOfRatings / this.totalVotes;
            this.totalRatings = this.totalVotes;
          } else {
            this.rate = 0
          }
          this.updateStars();
        })
    }
  }

  updateRateValue(i: number) {
    if (!this.isDisabled) {
      let profileModal = this.modal.create(RatingModal, {
        ratingValue: i,
        uid: this.args.uid,
        eventId: this.args.eventId,
        username: this.username
      });
      profileModal.present();

      profileModal.onDidDismiss((status: any) => {
        this.isFeedbackExists = status;
        this.checkFeedbackExists();
      });
    }
    else {
      this.helperService.showMessage("Attend an event to submit feedback.", 3000);
    }
  }

  checkFeedbackExists() {
    this.af.database.object('/feedbacks/' + this.args.eventId + '/' + this.args.uid, { preserveSnapshot: true })
      .take(1)
      .subscribe((feedback) => {
        if (feedback.val() !== null && this.args.ratingEnabled) {
          this.isFeedbackExists = true;
          this.myFeedback = feedback.val();
        }
      });
  }

  updateStars() {
    for (let i = 0; i < 5; i++) {
      this.icons[i] = 'star-outline'
    }
    for (let i = 0; i < this.rate; i++) {
      this.icons[i] = 'star';
    }
    if ((this.rate % 1) !== 0) {
      this.icons[parseInt(this.rate)] = 'star-half';
    }
  }
}
