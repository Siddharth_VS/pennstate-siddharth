import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Talisma } from '../../providers/crm/talisma';
import { SpeakerDetail } from '../../pages/speakers/speaker-detail';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { AngularFire } from 'angularfire2'

@Component({
  selector: 'speaker-card',
  templateUrl: 'speaker-card.html'
})

export class SpeakerCard {

  @Input('params') event: any;
  @Input('participationStatus') participationStatus: any;
  speakers: any[] = [];

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public af:AngularFire,
    public talisma: Talisma,
    public navCtrl: NavController,
  ) { }

  ngOnChanges() {
      this.af.database.list('/speakers/', { query: { orderByChild: 'Events/'+this.event.EventId, equalTo: ''+this.event.EventId} })
      .take(1)
      .subscribe((events: any) => {
        this.speakers = events;
      })
  }

  viewSpeker(speaker: any) {
    this.navCtrl.push(SpeakerDetail, { speaker: speaker, participationStatus: this.participationStatus });
  }
}
