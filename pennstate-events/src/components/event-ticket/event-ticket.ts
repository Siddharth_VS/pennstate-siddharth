import { Component,  Renderer } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'event-ticket',
  templateUrl: 'event-ticket.html'
})
export class EventTicket {
  params: any;
  uid: string;
  user: any = {};
  eventId: string;
  event: any = {};

  constructor(
    private afoDatabase: AngularFireOfflineDatabase,
    public navParams: NavParams,
    public renderer: Renderer,
    private userData: UserData,
    public viewCtrl: ViewController
  ) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'custom-popup', true);
    this.eventId = this.navParams.get('eventId');
    this.userData.getUid()
      .then((uid: string) => {
        this.uid = uid;
        return this.userData.getUser();
      })
      .then((userObservable) => {
        userObservable.subscribe((user) => {
          this.user = user;
        })
      })
    this.afoDatabase.object('/events/' + this.eventId)
      .subscribe((data) => {
        this.event = data;
      })
  }
}
