import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { PostComponent } from '../../post.component';
import { HelperService } from '../../../providers/helperService';
import * as moment from 'moment-timezone';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'poll-post.html',
  selector: 'poll-post'
})

export class PollPostComponent implements PostComponent, OnInit, OnDestroy {
  @Input() post: any;
  @Input() params: any;
  pollValue: any;
  totalVotes: number = 0;
  index: any;
  timeZone: any;
  isPollExpired: boolean = false;
  countArray: any = [];
  uid: any;
  timeoutId: any;
  currentTime: any;
  expiryTime: any;

  constructor(
    private alertCtrl: AlertController,
    public helper: HelperService,
    public afoDatabase: AngularFireOfflineDatabase
  ) { }

  ngOnInit() {
    this.runOnInit();
  }

  runOnInit() {
    this.helper.getUid()
      .then((uid: string) => {
        this.uid = uid;
        this.afoDatabase.object('/settings/timeZone')
          .take(1)
          .subscribe((zone: any) => {
            this.timeZone = zone.$value;
            this.checkPollStatus();
            this.checkPollExpiry();
            if (!this.isPollExpired) {
              this.checkExpiryOnTimeOut();
            }
            this.pollValue = this.post.options[0].name;
          });
      });
  }

  checkPollStatus() {
    for (let i = 0; i < this.post.options.length; i++) {
      if (!this.post.options[i].users) {
        this.post.options[i].users = {}
      }
      if (this.post.options[i].users[this.uid]) {
        this.isPollExpired = true;
      }
      this.countArray[i] = Object.keys(this.post.options[i].users).length;
      this.totalVotes += this.countArray[i];
    }
  }

  checkPollExpiry() {
    this.currentTime = parseInt(moment.utc().format('x'));
    this.expiryTime = this.post.expiryTime;
    if (this.expiryTime < this.currentTime) {
      this.isPollExpired = true;
    }
  }

  checkExpiryOnTimeOut() {
    this.timeoutId = setTimeout(() => {
      this.checkPollExpiry();
      clearTimeout(this.timeoutId)
    }, (this.expiryTime - this.currentTime));
  }

  submit() {
    for (let i = 0; i < this.post.options.length; i++) {
      if (this.post.options[i].name === this.pollValue) {
        this.index = i;
      }
    }
    if (!this.isPollExpired) {
      if (this.post.options[this.index]) {
        if (!this.post.options[this.index].users) {
          this.post.options[this.index].users = {}
        }
        this.post.options[this.index].users[this.uid] = true;
        this.afoDatabase.object('/posts/' + this.post.id + '/options')
          .set(this.post.options)
          .then(() => {
            this.isPollExpired = true;
            this.helper.showMessage('Thanks for voting!');
            this.checkPollStatus();
          });
      }
    }
  }

  ngOnDestroy() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId)
    }
  }
}
